/*
 * Please do not edit this file.
 * It was generated using rpcgen.
 */

#include "cloud_vmi.h"
#include <stdio.h>
#include <stdlib.h>
#include <rpc/pmap_clnt.h>
#include <string.h>
#include <memory.h>
#include <sys/socket.h>
#include <netinet/in.h>

#ifndef SIG_PF
#define SIG_PF void(*)(int)
#endif

static void
cloud_vmi_prog_1(struct svc_req *rqstp, register SVCXPRT *transp)
{
	union {
		vmi_init_in vmi_init_remote_1_arg;
		vmi_destroy_in vmi_destroy_remote_1_arg;
		vmi_pause_vm_in vmi_pause_vm_remote_1_arg;
		vmi_resume_vm_in vmi_resume_vm_remote_1_arg;
		vmi_get_memsize_in vmi_get_memsize_remote_1_arg;
		vmi_get_ostype_in vmi_get_ostype_remote_1_arg;
		vmi_read_addr_ksym_in vmi_read_addr_ksym_remote_1_arg;
		vmi_read_addr_va_in vmi_read_addr_va_remote_1_arg;
		vmi_get_page_mode_in vmi_get_page_mode_remote_1_arg;
		vmi_read_str_va_in vmi_read_str_va_remote_1_arg;
		vmi_read_unicode_str_va_in vmi_read_unicode_str_va_remote_1_arg;
		vmi_convert_str_encoding_in vmi_convert_str_encoding_remote_1_arg;
		vmi_get_offset_in vmi_get_offset_remote_1_arg;
		vmi_get_name_in vmi_get_name_remote_1_arg;
		vmi_get_access_mode_in vmi_get_access_mode_remote_1_arg;
		vmi_get_vmid_in vmi_get_vmid_remote_1_arg;
		vmi_translate_ksym2v_in vmi_translate_ksym2v_remote_1_arg;
		vmi_read_32_va_in vmi_read_32_va_remote_1_arg;
		send_vm_info_in send_vm_info_1_arg;
	} argument;
	char *result;
	xdrproc_t _xdr_argument, _xdr_result;
	char *(*local)(char *, struct svc_req *);

	switch (rqstp->rq_proc) {
	case NULLPROC:
		(void) svc_sendreply (transp, (xdrproc_t) xdr_void, (char *)NULL);
		return;

	case VMI_INIT_REMOTE:
		_xdr_argument = (xdrproc_t) xdr_vmi_init_in;
		_xdr_result = (xdrproc_t) xdr_vmi_init_out;
		local = (char *(*)(char *, struct svc_req *)) vmi_init_remote_1_svc;
		break;

	case VMI_DESTROY_REMOTE:
		_xdr_argument = (xdrproc_t) xdr_vmi_destroy_in;
		_xdr_result = (xdrproc_t) xdr_vmi_destroy_out;
		local = (char *(*)(char *, struct svc_req *)) vmi_destroy_remote_1_svc;
		break;

	case VMI_PAUSE_VM_REMOTE:
		_xdr_argument = (xdrproc_t) xdr_vmi_pause_vm_in;
		_xdr_result = (xdrproc_t) xdr_vmi_pause_vm_out;
		local = (char *(*)(char *, struct svc_req *)) vmi_pause_vm_remote_1_svc;
		break;

	case VMI_RESUME_VM_REMOTE:
		_xdr_argument = (xdrproc_t) xdr_vmi_resume_vm_in;
		_xdr_result = (xdrproc_t) xdr_vmi_resume_vm_out;
		local = (char *(*)(char *, struct svc_req *)) vmi_resume_vm_remote_1_svc;
		break;

	case VMI_GET_MEMSIZE_REMOTE:
		_xdr_argument = (xdrproc_t) xdr_vmi_get_memsize_in;
		_xdr_result = (xdrproc_t) xdr_vmi_get_memsize_out;
		local = (char *(*)(char *, struct svc_req *)) vmi_get_memsize_remote_1_svc;
		break;

	case PRINT_RESULT_REMOTE:
		_xdr_argument = (xdrproc_t) xdr_void;
		_xdr_result = (xdrproc_t) xdr_print_result_out;
		local = (char *(*)(char *, struct svc_req *)) print_result_remote_1_svc;
		break;

	case RESET_SERVER_TIMERS:
		_xdr_argument = (xdrproc_t) xdr_void;
		_xdr_result = (xdrproc_t) xdr_reset_server_timers_out;
		local = (char *(*)(char *, struct svc_req *)) reset_server_timers_1_svc;
		break;

	case VMI_GET_OSTYPE_REMOTE:
		_xdr_argument = (xdrproc_t) xdr_vmi_get_ostype_in;
		_xdr_result = (xdrproc_t) xdr_vmi_get_ostype_out;
		local = (char *(*)(char *, struct svc_req *)) vmi_get_ostype_remote_1_svc;
		break;

	case VMI_READ_ADDR_KSYM_REMOTE:
		_xdr_argument = (xdrproc_t) xdr_vmi_read_addr_ksym_in;
		_xdr_result = (xdrproc_t) xdr_vmi_read_addr_ksym_out;
		local = (char *(*)(char *, struct svc_req *)) vmi_read_addr_ksym_remote_1_svc;
		break;

	case VMI_READ_ADDR_VA_REMOTE:
		_xdr_argument = (xdrproc_t) xdr_vmi_read_addr_va_in;
		_xdr_result = (xdrproc_t) xdr_vmi_read_addr_va_out;
		local = (char *(*)(char *, struct svc_req *)) vmi_read_addr_va_remote_1_svc;
		break;

	case VMI_GET_PAGE_MODE_REMOTE:
		_xdr_argument = (xdrproc_t) xdr_vmi_get_page_mode_in;
		_xdr_result = (xdrproc_t) xdr_vmi_get_page_mode_out;
		local = (char *(*)(char *, struct svc_req *)) vmi_get_page_mode_remote_1_svc;
		break;

	case VMI_READ_STR_VA_REMOTE:
		_xdr_argument = (xdrproc_t) xdr_vmi_read_str_va_in;
		_xdr_result = (xdrproc_t) xdr_vmi_read_str_va_out;
		local = (char *(*)(char *, struct svc_req *)) vmi_read_str_va_remote_1_svc;
		break;

	case VMI_READ_UNICODE_STR_VA_REMOTE:
		_xdr_argument = (xdrproc_t) xdr_vmi_read_unicode_str_va_in;
		_xdr_result = (xdrproc_t) xdr_vmi_read_unicode_str_va_out;
		local = (char *(*)(char *, struct svc_req *)) vmi_read_unicode_str_va_remote_1_svc;
		break;

	case VMI_CONVERT_STR_ENCODING_REMOTE:
		_xdr_argument = (xdrproc_t) xdr_vmi_convert_str_encoding_in;
		_xdr_result = (xdrproc_t) xdr_vmi_convert_str_encoding_out;
		local = (char *(*)(char *, struct svc_req *)) vmi_convert_str_encoding_remote_1_svc;
		break;

	case VMI_GET_OFFSET_REMOTE:
		_xdr_argument = (xdrproc_t) xdr_vmi_get_offset_in;
		_xdr_result = (xdrproc_t) xdr_vmi_get_offset_out;
		local = (char *(*)(char *, struct svc_req *)) vmi_get_offset_remote_1_svc;
		break;

	case VMI_GET_NAME_REMOTE:
		_xdr_argument = (xdrproc_t) xdr_vmi_get_name_in;
		_xdr_result = (xdrproc_t) xdr_vmi_get_name_out;
		local = (char *(*)(char *, struct svc_req *)) vmi_get_name_remote_1_svc;
		break;

	case VMI_GET_ACCESS_MODE_REMOTE:
		_xdr_argument = (xdrproc_t) xdr_vmi_get_access_mode_in;
		_xdr_result = (xdrproc_t) xdr_vmi_get_access_mode_out;
		local = (char *(*)(char *, struct svc_req *)) vmi_get_access_mode_remote_1_svc;
		break;

	case VMI_GET_VMID_REMOTE:
		_xdr_argument = (xdrproc_t) xdr_vmi_get_vmid_in;
		_xdr_result = (xdrproc_t) xdr_vmi_get_vmid_out;
		local = (char *(*)(char *, struct svc_req *)) vmi_get_vmid_remote_1_svc;
		break;

	case VMI_TRANSLATE_KSYM2V_REMOTE:
		_xdr_argument = (xdrproc_t) xdr_vmi_translate_ksym2v_in;
		_xdr_result = (xdrproc_t) xdr_vmi_translate_ksym2v_out;
		local = (char *(*)(char *, struct svc_req *)) vmi_translate_ksym2v_remote_1_svc;
		break;

	case VMI_READ_32_VA_REMOTE:
		_xdr_argument = (xdrproc_t) xdr_vmi_read_32_va_in;
		_xdr_result = (xdrproc_t) xdr_vmi_read_32_va_out;
		local = (char *(*)(char *, struct svc_req *)) vmi_read_32_va_remote_1_svc;
		break;

	case SEND_VM_INFO:
		_xdr_argument = (xdrproc_t) xdr_send_vm_info_in;
		_xdr_result = (xdrproc_t) xdr_send_vm_info_out;
		local = (char *(*)(char *, struct svc_req *)) send_vm_info_1_svc;
		break;

	default:
		svcerr_noproc (transp);
		return;
	}
	memset ((char *)&argument, 0, sizeof (argument));
	if (!svc_getargs (transp, (xdrproc_t) _xdr_argument, (caddr_t) &argument)) {
		svcerr_decode (transp);
		return;
	}
	result = (*local)((char *)&argument, rqstp);
	if (result != NULL && !svc_sendreply(transp, (xdrproc_t) _xdr_result, result)) {
		svcerr_systemerr (transp);
	}
	if (!svc_freeargs (transp, (xdrproc_t) _xdr_argument, (caddr_t) &argument)) {
		fprintf (stderr, "%s", "unable to free arguments");
		exit (1);
	}
	return;
}

int
main (int argc, char **argv)
{
	register SVCXPRT *transp;

	pmap_unset (CLOUD_VMI_PROG, CLOUD_VMI_VERS);

	transp = svcudp_create(RPC_ANYSOCK);
	if (transp == NULL) {
		fprintf (stderr, "%s", "cannot create udp service.");
		exit(1);
	}
	if (!svc_register(transp, CLOUD_VMI_PROG, CLOUD_VMI_VERS, cloud_vmi_prog_1, IPPROTO_UDP)) {
		fprintf (stderr, "%s", "unable to register (CLOUD_VMI_PROG, CLOUD_VMI_VERS, udp).");
		exit(1);
	}

	transp = svctcp_create(RPC_ANYSOCK, 0, 0);
	if (transp == NULL) {
		fprintf (stderr, "%s", "cannot create tcp service.");
		exit(1);
	}
	if (!svc_register(transp, CLOUD_VMI_PROG, CLOUD_VMI_VERS, cloud_vmi_prog_1, IPPROTO_TCP)) {
		fprintf (stderr, "%s", "unable to register (CLOUD_VMI_PROG, CLOUD_VMI_VERS, tcp).");
		exit(1);
	}

	svc_run ();
	fprintf (stderr, "%s", "svc_run returned");
	exit (1);
	/* NOTREACHED */
}
