/*--------------- in data structures---------------*/

struct vmi_type_0_in_data {
	string vmi_key<>;
};
struct vmi_type_1_in_data {
	uint32_t flags;
	string name<>;
};
struct vmi_type_2_in_data {
	string vmi_key<>;
	string sym<>;	
};
struct vmi_type_3_in_data {
	string vmi_key<>;
	uint64_t vaddr;
	int pid;
};
struct vmi_type_4_in_data {
	uint8_t   contents<>;
	string  in_encoding<>;
	string  out_encoding<>;
};
struct vmi_type_5_in_data {
	string vmi_key<>;
	string offset_name<>;
};
struct vmi_type_6_in_data {
    string libvmi_conf<>;
};

/*---------------out data structures---------------*/

struct vmi_type_0_out_data {
	int status;
	string vmi_key<>;
	unsigned int server_status;
};
struct vmi_type_1_out_data {
	int status;
	uint64_t value; 
	unsigned int server_status;
};
struct vmi_type_2_out_data {
	uint8_t contents<>;
	string  encoding<>;
	unsigned int server_status;
};
struct vmi_type_3_out_data {
	int status;
	uint8_t contents<>;
	string  encoding<>;
	unsigned int server_status;
};
struct vmi_type_4_out_data {
	int status;
	uint32_t value;
	unsigned int server_status;
};
struct vmi_type_5_out_data {
	int rtn;
	unsigned int server_status;
};
struct vmi_type_6_out_data {
	unsigned long rtn;
	unsigned int server_status;
};
struct vmi_type_7_out_data {
	string rtn<>;
	unsigned int server_status;
};
struct vmi_type_8_out_data {
	uint32_t rtn;
	unsigned int server_status;
};
struct vmi_type_9_out_data {
	uint64_t rtn;
	unsigned int server_status;
};


/*-- output parameters --*/
typedef struct vmi_type_0_out_data vmi_init_out ;
typedef struct vmi_type_5_out_data vmi_destroy_out ;
typedef struct vmi_type_5_out_data vmi_pause_vm_out ;
typedef struct vmi_type_5_out_data vmi_resume_vm_out ;
typedef struct vmi_type_6_out_data vmi_get_memsize_out ;
typedef struct vmi_type_7_out_data print_result_out ;
typedef struct vmi_type_5_out_data reset_server_timers_out ;
typedef struct vmi_type_5_out_data vmi_get_ostype_out ;
typedef struct vmi_type_1_out_data vmi_read_addr_ksym_out ;
typedef struct vmi_type_1_out_data vmi_read_addr_va_out ;
typedef struct vmi_type_5_out_data vmi_get_page_mode_out ;
typedef struct vmi_type_7_out_data vmi_read_str_va_out ;
typedef struct vmi_type_2_out_data vmi_read_unicode_str_va_out ;
typedef struct vmi_type_3_out_data vmi_convert_str_encoding_out ;
typedef struct vmi_type_6_out_data vmi_get_offset_out ;
typedef struct vmi_type_7_out_data vmi_get_name_out ;
typedef struct vmi_type_8_out_data vmi_get_access_mode_out ;
typedef struct vmi_type_6_out_data vmi_get_vmid_out ;
typedef struct vmi_type_9_out_data vmi_translate_ksym2v_out ;
typedef struct vmi_type_4_out_data vmi_read_32_va_out ;
typedef struct vmi_type_5_out_data send_vm_info_out;

/*-- input parameters --*/
typedef struct vmi_type_1_in_data vmi_init_in ;
typedef struct vmi_type_0_in_data vmi_destroy_in ;
typedef struct vmi_type_0_in_data vmi_pause_vm_in ;
typedef struct vmi_type_0_in_data vmi_resume_vm_in ;
typedef struct vmi_type_0_in_data vmi_get_memsize_in ;
/*typedef struct vmi_type_x_in_data print_result_in ;*/
/*typedef struct vmi_type_x_in_data reset_server_timers_in ;*/
typedef struct vmi_type_0_in_data vmi_get_ostype_in ;
typedef struct vmi_type_2_in_data vmi_read_addr_ksym_in ;
typedef struct vmi_type_3_in_data vmi_read_addr_va_in ;
typedef struct vmi_type_0_in_data vmi_get_page_mode_in ;
typedef struct vmi_type_3_in_data vmi_read_str_va_in ;
typedef struct vmi_type_3_in_data vmi_read_unicode_str_va_in ;
typedef struct vmi_type_4_in_data vmi_convert_str_encoding_in ;
typedef struct vmi_type_5_in_data vmi_get_offset_in ;
typedef struct vmi_type_0_in_data vmi_get_name_in ;
typedef struct vmi_type_0_in_data vmi_get_access_mode_in ;
typedef struct vmi_type_0_in_data vmi_get_vmid_in ;
typedef struct vmi_type_2_in_data vmi_translate_ksym2v_in ;
typedef struct vmi_type_3_in_data vmi_read_32_va_in ;
typedef struct vmi_type_6_in_data send_vm_info_in;

program CLOUD_VMI_PROG {
    version CLOUD_VMI_VERS {
		vmi_init_out	VMI_INIT_REMOTE ( vmi_init_in ) = 1;
		vmi_destroy_out	VMI_DESTROY_REMOTE ( vmi_destroy_in ) = 2;
		vmi_pause_vm_out	VMI_PAUSE_VM_REMOTE ( vmi_pause_vm_in ) = 3;
		vmi_resume_vm_out	VMI_RESUME_VM_REMOTE ( vmi_resume_vm_in ) = 4;
		vmi_get_memsize_out	VMI_GET_MEMSIZE_REMOTE	( vmi_get_memsize_in ) = 5;
		print_result_out	PRINT_RESULT_REMOTE ( ) = 6;
		reset_server_timers_out	RESET_SERVER_TIMERS () = 7;
		vmi_get_ostype_out	VMI_GET_OSTYPE_REMOTE ( vmi_get_ostype_in ) = 8;
		vmi_read_addr_ksym_out	VMI_READ_ADDR_KSYM_REMOTE	( vmi_read_addr_ksym_in ) = 9;
		vmi_read_addr_va_out	VMI_READ_ADDR_VA_REMOTE ( vmi_read_addr_va_in ) = 10;
		vmi_get_page_mode_out	VMI_GET_PAGE_MODE_REMOTE ( vmi_get_page_mode_in ) = 11;
		vmi_read_str_va_out	VMI_READ_STR_VA_REMOTE ( vmi_read_str_va_in ) = 12;
		vmi_read_unicode_str_va_out	VMI_READ_UNICODE_STR_VA_REMOTE ( vmi_read_unicode_str_va_in ) = 13;
		vmi_convert_str_encoding_out	VMI_CONVERT_STR_ENCODING_REMOTE	( vmi_convert_str_encoding_in ) = 14;
		vmi_get_offset_out	VMI_GET_OFFSET_REMOTE ( vmi_get_offset_in ) = 15;
		vmi_get_name_out	VMI_GET_NAME_REMOTE ( vmi_get_name_in ) = 16;
		vmi_get_access_mode_out	VMI_GET_ACCESS_MODE_REMOTE ( vmi_get_access_mode_in ) = 17;
		vmi_get_vmid_out	VMI_GET_VMID_REMOTE ( vmi_get_vmid_in ) = 18;
		vmi_translate_ksym2v_out	VMI_TRANSLATE_KSYM2V_REMOTE ( vmi_translate_ksym2v_in ) = 19;
		vmi_read_32_va_out	VMI_READ_32_VA_REMOTE ( vmi_read_32_va_in ) = 20;
		send_vm_info_out SEND_VM_INFO ( send_vm_info_in ) = 21;
    } = 1;
} = 19850724;