//Server application.
// Deleted average_1() from the
// source http://www.linuxjournal.com/article/2204?page=0,1
#include "custom_config.h"

#include <libvmi/libvmi.h>
#include <rpc/rpc.h>
#include "cloud_vmi.h"
#include "customlib.h"

#include <iostream>
#include <sstream>
#include <string>
#include <cstring>
#include <map>
#include <pwd.h>
#include <stdlib.h>
#include <sys/types.h>

#define MAX_VMI_KEY_SIZE 200

using namespace std;

static TIMER_C total_timer, libvmi_timer, vmi_init_timer, destroy_timer, get_memsize_timer, pause_vm_timer, resume_vm_timer;
static unsigned char is_first = 1;
static void init_all_timers() {
	init_timer(&total_timer);
	init_timer(&libvmi_timer);
	init_timer(&vmi_init_timer);
	init_timer(&destroy_timer);
	init_timer(&get_memsize_timer);
	init_timer(&pause_vm_timer);
	init_timer(&resume_vm_timer);
	is_first = 0;
}
static string print_timers() {
	double libvmi_total_time = 0;
	stringstream ss;
	ss << "-------------------Remote Info-------------------\n";
	ss << "Total server time " << get_total_time(&total_timer) <<'\n';
	if(get_total_time(&vmi_init_timer)) {
		libvmi_total_time +=get_total_time(&vmi_init_timer);
		ss << "Init time " << get_total_time(&vmi_init_timer) <<'\n';
	}
	if(get_total_time(&destroy_timer)) {
		libvmi_total_time +=get_total_time(&destroy_timer);
		ss << "Destroy time " << get_total_time(&destroy_timer) <<'\n';
	}
	if(get_total_time(&get_memsize_timer)) {
		libvmi_total_time +=get_total_time(&get_memsize_timer);
		ss << "Get Memsize time " << get_total_time(&get_memsize_timer) <<'\n';
	}
	if(get_total_time(&pause_vm_timer)) {
		libvmi_total_time +=get_total_time(&pause_vm_timer);
		ss << "Pause VM time " << get_total_time(&pause_vm_timer) <<'\n';
	}
	if(get_total_time(&resume_vm_timer)) {
		libvmi_total_time +=get_total_time(&resume_vm_timer);
		ss << "Resume VM time " << get_total_time(&resume_vm_timer) <<'\n';
	}
	ss << "Total libvmi time " << libvmi_total_time <<'\n';
	return ss.str();
}

map <string, vmi_instance_t> vmi_map;
reset_server_timers_out * reset_server_timers_1_svc(void * input, struct svc_req * svc) {
	static reset_server_timers_out output;
	output.server_status = 0;
	output.rtn = 0;
	init_all_timers();
	return &output;
}

/*This function origins from open_config_file() at core.c of libvmi*/
static FILE * make_config_file( )
{
    FILE *f = NULL;
    char location[100];
    char *sudo_user = NULL;
    struct passwd *pw_entry = NULL;

    /* first check home directory of sudo user */
    if ((sudo_user = getenv("SUDO_USER")) != NULL) {
        if ((pw_entry = getpwnam(sudo_user)) != NULL) {
            snprintf(location, 100, "%s/etc/libvmi.conf\0",
                     pw_entry->pw_dir);
            //dbprint(VMI_DEBUG_CORE, "--looking for config file at %s\n", location);
            if ((f = fopen(location, "w")) != NULL) {
                goto success;
            }
        }
    }

    /* next check home directory for current user */
    snprintf(location, 100, "%s/etc/libvmi.conf\0", getenv("HOME"));
    //dbprint(VMI_DEBUG_CORE, "--looking for config file at %s\n", location);
    if ((f = fopen(location, "w")) != NULL) {
        goto success;
    }

    /* finally check in /etc */
    snprintf(location, 100, "/etc/libvmi.conf\0");
    //dbprint(VMI_DEBUG_CORE, "--looking for config file at %s\n", location);
    if ((f = fopen(location, "w")) != NULL) {
        goto success;
    }

    return NULL;
success:
    //dbprint(VMI_DEBUG_CORE, "**Using config file at %s\n", location);
    return f;
}

send_vm_info_out * send_vm_info_1_svc(send_vm_info_in *input, struct svc_req *svc) {
	FILE * f;
	static send_vm_info_out output;
	output.server_status = 0;
	output.rtn = VMI_SUCCESS;
	size_t written_size;
	if (input->libvmi_conf == NULL || sizeof(input->libvmi_conf) < 1 ) {
		output.rtn = VMI_FAILURE;
		output.server_status = 1;
		return &output;
	}
	f = make_config_file();
	if (f == NULL) {
		output.rtn = VMI_FAILURE;
		output.server_status = 1;
		return &output;
	}
	//printf("sent libvmi.conf : %s\n",input->libvmi_conf);
	//cout << "file transferred\n";
	written_size = fwrite (input->libvmi_conf, sizeof(char), strlen((char*)(input->libvmi_conf)), f);
	if ( written_size != strlen(input->libvmi_conf) ) {
		cout << "Failed to write all the configuration contents\n";
		fclose(f);
		output.rtn = VMI_FAILURE;
		output.server_status = 1;
		return &output;
	}
	cout << "remote libvmi.conf has been transferred.\n";
	fclose(f);
	return &output;
}

print_result_out * print_result_remote_1_svc(void * input, struct svc_req * svc) {
	static print_result_out output;
	output.server_status = 0;
	if(output.rtn!=NULL) delete[] output.rtn;
	string rtn_string = print_timers();
	char* cstr = new char[rtn_string.length() + 1];
	copy(rtn_string.begin(), rtn_string.end(), cstr);
	cstr[rtn_string.length()] ='\0';
	output.rtn = cstr;
	return &output;
}
vmi_init_out * vmi_init_remote_1_svc(vmi_init_in *input, struct svc_req *svc) {
	static vmi_init_out output;
	output.server_status = 0;
	if(is_first) init_all_timers();
	start_timer(&total_timer);
	vmi_instance_t vmi;
	if (output.vmi_key != NULL) { // Freeing previous data
		delete[] output.vmi_key;
		output.vmi_key = NULL;
	}
	if(PRINT_DBG_MSG) cout << "vmi_init()\n";
	if(PRINT_DBG_MSG) cout << "vm name : " << input->name << " flag : " << input->flags << "\n";
	stringstream ss;
	ss << input->name << (input->flags);
	string vmi_key = ss.str();
	char* cstr = new char[vmi_key.length() + 1];
	copy(vmi_key.begin(), vmi_key.end(), cstr);
	cstr[vmi_key.length()] ='\0';
	output.vmi_key = cstr;
	if(vmi_map[vmi_key] != NULL) {
		if(PRINT_DBG_MSG) cout << "vmi already exists\n";
		output.status = VMI_SUCCESS;
		end_timer(&total_timer);
		return &output;
	}
	start_timer(&vmi_init_timer);
	output.status = vmi_init(&vmi, input->flags, input->name);
	end_timer(&vmi_init_timer);
	if ( output.status == VMI_SUCCESS ) {
		vmi_map[vmi_key] = vmi;
	}
	end_timer(&total_timer);
	return &output;
}

vmi_destroy_out * vmi_destroy_remote_1_svc(vmi_destroy_in *input, struct svc_req *svc) {
	static vmi_destroy_out output;
	output.server_status = 0;
	if(is_first) init_all_timers();
	start_timer(&total_timer);
	if(PRINT_DBG_MSG) cout << "vmi_destroy()\n";
	vmi_instance_t vmi = vmi_map[string(input->vmi_key)];
	if(vmi==NULL) {
		cerr << "Unable to find the vmi keyed by " << input->vmi_key << " \n";
		output.server_status = 1;
		end_timer(&total_timer);
		return &output;
	}
	vmi_map.erase(string(input->vmi_key));
	start_timer(&destroy_timer);
	output.rtn = vmi_destroy(vmi);
	end_timer(&destroy_timer);
	end_timer(&total_timer);
	return &output;
}

vmi_pause_vm_out * vmi_pause_vm_remote_1_svc(vmi_pause_vm_in *input, struct svc_req *svc) {
	static vmi_pause_vm_out output;
	output.server_status = 0;
	if(is_first) init_all_timers();
	start_timer(&total_timer);
	if(PRINT_DBG_MSG) cout << "vmi_pause_vm()\n";
	vmi_instance_t vmi = vmi_map[string(input->vmi_key)];
	if(vmi==NULL) {
		cerr << "Unable to find the vmi keyed by " << input->vmi_key << " \n";
		output.server_status = 1;
		end_timer(&total_timer);
		return &output;
	}
	output.rtn = VMI_SUCCESS;
	start_timer(&pause_vm_timer);
	output.rtn = vmi_pause_vm(vmi);
	end_timer(&pause_vm_timer);
	end_timer(&total_timer);
	return &output;
}

vmi_resume_vm_out * vmi_resume_vm_remote_1_svc(vmi_resume_vm_in *input, struct svc_req *svc) {
	static vmi_resume_vm_out output;
	output.server_status = 0;
	if(is_first) init_all_timers();
	start_timer(&total_timer);
	if(PRINT_DBG_MSG) cout << "vmi_resume_vm()\n";
	vmi_instance_t vmi = vmi_map[string(input->vmi_key)];
	if(vmi==NULL) {
		cerr << "Unable to find the vmi keyed by " << input->vmi_key << " \n";
		output.server_status = 1;
		end_timer(&total_timer);
		return &output;
	}
	start_timer(&resume_vm_timer);
	output.rtn = vmi_resume_vm(vmi);
	end_timer(&resume_vm_timer);
	end_timer(&total_timer);
	return &output;
}

vmi_get_memsize_out * vmi_get_memsize_remote_1_svc(vmi_get_memsize_in *input, struct svc_req *svc) {
	static vmi_get_memsize_out output;
	output.server_status = 0;
	if(is_first) init_all_timers();
	start_timer(&total_timer);
	if(PRINT_DBG_MSG) cout << "vmi_get_memsize()\n";
	vmi_instance_t vmi = vmi_map[string(input->vmi_key)];
	if(vmi==NULL) {
		cerr << "Unable to find the vmi keyed by " << input->vmi_key << " \n";
		output.rtn = VMI_FAILURE;
		output.server_status = 1;
		end_timer(&total_timer);
		return &output;
	}
	start_timer(&get_memsize_timer);
	output.rtn = vmi_get_memsize(vmi);
	end_timer(&get_memsize_timer);
	end_timer(&total_timer);
	return &output;
}

vmi_get_ostype_out * vmi_get_ostype_remote_1_svc(vmi_get_ostype_in * input, struct svc_req * svc) {
	static vmi_get_ostype_out output;
	output.server_status = 0;
	if(is_first) init_all_timers();
	start_timer(&total_timer);
	if(PRINT_DBG_MSG) cout << "vmi_get_ostype()\n";
	vmi_instance_t vmi = vmi_map[string(input->vmi_key)];
	if(vmi==NULL) {
		cerr << "Unable to find the vmi keyed by " << input->vmi_key << " \n";
		output.server_status = 1;
		end_timer(&total_timer);
		return &output;
	}
	output.rtn = vmi_get_ostype(vmi);
	end_timer(&total_timer);
	return &output;
}
vmi_read_addr_ksym_out * vmi_read_addr_ksym_remote_1_svc(vmi_read_addr_ksym_in * input, struct svc_req * svc) {
	static vmi_read_addr_ksym_out output;
	output.server_status = 0;
	if(is_first) init_all_timers();
	start_timer(&total_timer);
	if(PRINT_DBG_MSG) cout << "vmi_read_addr_ksym()\n";
	vmi_instance_t vmi = vmi_map[string(input->vmi_key)];
	if(vmi==NULL || input->vmi_key == NULL || input->sym == NULL) {
		if (input->vmi_key == NULL) cerr << "NULL cannot be a valid vmi_key.\n";
		else if (vmi == NULL) cerr << "Unable to find the vmi keyed by " << input->vmi_key << " \n";
		if (input->sym == NULL) cerr << "NULL cannot be a valid symbol.\n";
		output.server_status = 1;
		end_timer(&total_timer);
		return &output;
	}
	output.status = vmi_read_addr_ksym(vmi,input->sym,&(output.value));
	end_timer(&total_timer);
	return &output;
}
vmi_read_addr_va_out * vmi_read_addr_va_remote_1_svc(vmi_read_addr_va_in * input, struct svc_req * svc) {
	static vmi_read_addr_va_out output;
	output.server_status = 0;
	if(is_first) init_all_timers();
	start_timer(&total_timer);
	if(PRINT_DBG_MSG) cout << "vmi_read_addr_va()\n";
	vmi_instance_t vmi = vmi_map[string(input->vmi_key)];
	if(vmi==NULL || input->vmi_key == NULL ) {
		if (input->vmi_key == NULL) cerr << "NULL cannot be a valid vmi_key.\n";
		else if (vmi == NULL) cerr << "Unable to find the vmi keyed by " << input->vmi_key << " \n";
		output.server_status = 1;
		end_timer(&total_timer);
		return &output;
	}
	output.status = vmi_read_addr_va(vmi,input->vaddr,input->pid,&(output.value));
	end_timer(&total_timer);
	return &output;
}
vmi_get_page_mode_out * vmi_get_page_mode_remote_1_svc(vmi_get_page_mode_in * input, struct svc_req * svc) {
	static vmi_get_page_mode_out output;
	output.server_status = 0;
	if(is_first) init_all_timers();
	start_timer(&total_timer);
	if(PRINT_DBG_MSG) cout << "vmi_get_page_mode_out()\n";
	vmi_instance_t vmi = vmi_map[string(input->vmi_key)];
	if(vmi==NULL || input->vmi_key == NULL ) {
		if (input->vmi_key == NULL) cerr << "NULL cannot be a valid vmi_key.\n";
		else if (vmi == NULL) cerr << "Unable to find the vmi keyed by " << input->vmi_key << " \n";
		output.server_status = 1;
		end_timer(&total_timer);
		return &output;
	}
	output.rtn = vmi_get_page_mode(vmi);
	end_timer(&total_timer);
	return &output;
}
vmi_read_str_va_out * vmi_read_str_va_remote_1_svc(vmi_read_str_va_in * input, struct svc_req * svc) {
	static vmi_read_str_va_out output;
	output.server_status = 0;
	if(is_first) init_all_timers();
	start_timer(&total_timer);
	if(PRINT_DBG_MSG) cout << "vmi_read_str_va_out()\n";
	vmi_instance_t vmi = vmi_map[string(input->vmi_key)];
	if(vmi==NULL || input->vmi_key == NULL ) {
		if (input->vmi_key == NULL) cerr << "NULL cannot be a valid vmi_key.\n";
		else if (vmi == NULL) cerr << "Unable to find the vmi keyed by " << input->vmi_key << " \n";
		output.rtn = NULL;
		output.server_status = 1;
		end_timer(&total_timer);
		return &output;
	}
	output.rtn = vmi_read_str_va ( vmi, input->vaddr, input-> pid );
	end_timer(&total_timer);
	return &output;
}
vmi_read_unicode_str_va_out * vmi_read_unicode_str_va_remote_1_svc(vmi_read_unicode_str_va_in * input, struct svc_req * svc) {
	static vmi_read_unicode_str_va_out output;
	static unicode_string_t *us = NULL;
	if( us!= NULL ) vmi_free_unicode_str(us);
	if (output.encoding !=NULL ) free(output.encoding);
	output.server_status = 0;
	if(is_first) init_all_timers();
	start_timer(&total_timer);
	if(PRINT_DBG_MSG) cout << "vmi_read_unicode_str_va_out()\n";
	vmi_instance_t vmi = vmi_map[string(input->vmi_key)];
	if(vmi==NULL || input->vmi_key == NULL ) {
		if (input->vmi_key == NULL) cerr << "NULL cannot be a valid vmi_key.\n";
		else if (vmi == NULL) cerr << "Unable to find the vmi keyed by " << input->vmi_key << " \n";
		output.server_status = 1;
		output.contents.contents_val = NULL;
		output.contents.contents_len = 0;
		output.encoding = NULL;
		end_timer(&total_timer);
		return &output;
	}
	us = vmi_read_unicode_str_va( vmi,input->vaddr,input->pid);
	output.contents.contents_val = us->contents;
	output.contents.contents_len = us->length;
	output.encoding = strdup(us->encoding);
	end_timer(&total_timer);
	return &output;
}
vmi_convert_str_encoding_out * vmi_convert_str_encoding_remote_1_svc(vmi_convert_str_encoding_in * input, struct svc_req * svc) {
	static vmi_convert_str_encoding_out output;
	static unicode_string_t *output_us = NULL;
	if( output_us!= NULL ) vmi_free_unicode_str(output_us);
	output.server_status = 0;
	if(is_first) init_all_timers();
	start_timer(&total_timer);
	if(PRINT_DBG_MSG) cout << "vmi_convert_str_encoding_out()\n";
	if(input->in_encoding == NULL || input->out_encoding == NULL || input->contents.contents_val == NULL || input->contents.contents_len <= 0) {
		output.server_status = 1;
		if(input->in_encoding == NULL ) fprintf(stderr,"Input Encoding is not specified!\n");
		if(input->out_encoding == NULL ) fprintf(stderr,"Output Encoding is not specified!\n");
		if(input->contents.contents_val == NULL ) fprintf(stderr,"Contents are not specified!\n");
		if(input->contents.contents_len <= 0) fprintf(stderr,"Length of Contents should be larger than 1!\n");
		end_timer(&total_timer);
		return &output;
	}
	unicode_string_t input_us;
	input_us.contents = input->contents.contents_val;
	input_us.length = input->contents.contents_len;
	input_us.encoding = input->in_encoding;
	output.status = vmi_convert_str_encoding( &input_us, output_us, input->out_encoding);
	output.contents.contents_len = output_us->length;
	output.contents.contents_val = output_us->contents;
	output.encoding = strdup(output_us->encoding);
	end_timer(&total_timer);
	return &output;
}
vmi_get_offset_out * vmi_get_offset_remote_1_svc(vmi_get_offset_in * input, struct svc_req * svc) {
	static vmi_get_offset_out output;
	output.server_status = 0;
	if(is_first) init_all_timers();
	start_timer(&total_timer);
	if(PRINT_DBG_MSG) cout << "vmi_get_offset_out()\n";
	vmi_instance_t vmi = vmi_map[string(input->vmi_key)];
	if(vmi==NULL || input->vmi_key == NULL || input->offset_name == NULL) {
		if (input->vmi_key == NULL) cerr << "NULL cannot be a valid vmi_key.\n";
		else if (vmi == NULL) cerr << "Unable to find the vmi keyed by " << input->vmi_key << " \n";
		else if (input->offset_name == NULL) cerr << "Offset name is not specified\n";
		output.server_status = 1;
		end_timer(&total_timer);
		return &output;
	}
	output.rtn = vmi_get_offset(vmi, input->offset_name);
	end_timer(&total_timer);
	return &output;
}
vmi_get_name_out * vmi_get_name_remote_1_svc(vmi_get_name_in * input, struct svc_req * svc) {
	static vmi_get_name_out output;
	output.server_status = 0;
	if(is_first) init_all_timers();
	start_timer(&total_timer);
	if(PRINT_DBG_MSG) cout << "vmi_get_name_out()\n";
	vmi_instance_t vmi = vmi_map[string(input->vmi_key)];
	if(vmi==NULL || input->vmi_key == NULL ) {
		if (input->vmi_key == NULL) cerr << "NULL cannot be a valid vmi_key.\n";
		else if (vmi == NULL) cerr << "Unable to find the vmi keyed by " << input->vmi_key << " \n";
		output.server_status = 1;
		output.rtn = NULL;
		end_timer(&total_timer);
		return &output;
	}
	output.rtn = vmi_get_name( vmi );
	end_timer(&total_timer);
	return &output;
}
vmi_get_access_mode_out * vmi_get_access_mode_remote_1_svc(vmi_get_access_mode_in * input, struct svc_req * svc) {
	static vmi_get_access_mode_out output;
	output.server_status = 0;
	if(is_first) init_all_timers();
	start_timer(&total_timer);
	if(PRINT_DBG_MSG) cout << "vmi_get_access_mode_out()\n";
	vmi_instance_t vmi = vmi_map[string(input->vmi_key)];
	if(vmi==NULL || input->vmi_key == NULL ) {
		if (input->vmi_key == NULL) cerr << "NULL cannot be a valid vmi_key.\n";
		else if (vmi == NULL) cerr << "Unable to find the vmi keyed by " << input->vmi_key << " \n";
		output.server_status = 1;
		end_timer(&total_timer);
		return &output;
	}
	output.rtn = vmi_get_access_mode( vmi );
	end_timer(&total_timer);
	return &output;
}
vmi_get_vmid_out * vmi_get_vmid_remote_1_svc(vmi_get_vmid_in * input, struct svc_req * svc) {
	static vmi_get_vmid_out output;
	output.server_status = 0;
	if(is_first) init_all_timers();
	start_timer(&total_timer);
	if(PRINT_DBG_MSG) cout << "vmi_get_vmid_out()\n";
	vmi_instance_t vmi = vmi_map[string(input->vmi_key)];
	if(vmi==NULL || input->vmi_key == NULL ) {
		if (input->vmi_key == NULL) cerr << "NULL cannot be a valid vmi_key.\n";
		else if (vmi == NULL) cerr << "Unable to find the vmi keyed by " << input->vmi_key << " \n";
		output.server_status = 1;
		end_timer(&total_timer);
		return &output;
	}
	output.rtn = vmi_get_vmid( vmi );
	end_timer(&total_timer);
	return &output;

}
vmi_translate_ksym2v_out * vmi_translate_ksym2v_remote_1_svc(vmi_translate_ksym2v_in * input, struct svc_req * svc) {
	static vmi_translate_ksym2v_out output;
	output.server_status = 0;
	if(is_first) init_all_timers();
	start_timer(&total_timer);
	if(PRINT_DBG_MSG) cout << "vmi_translate_ksym2v_out()\n";
	vmi_instance_t vmi = vmi_map[string(input->vmi_key)];
	if(vmi==NULL || input->vmi_key == NULL || input->sym == NULL) {
		if (input->vmi_key == NULL) cerr << "NULL cannot be a valid vmi_key.\n";
		else if (vmi == NULL) cerr << "Unable to find the vmi keyed by " << input->vmi_key << " \n";
		else if (input->sym == NULL) cerr << "Symbol parameter is NULL.\n";
		output.server_status = 1;
		end_timer(&total_timer);
		return &output;
	}
	output.rtn = vmi_translate_ksym2v( vmi, input->sym );
	end_timer(&total_timer);
	return &output;
}
vmi_read_32_va_out * vmi_read_32_va_remote_1_svc(vmi_read_32_va_in * input, struct svc_req * svc) {
	static vmi_read_32_va_out output;
	output.server_status = 0;
	if(is_first) init_all_timers();
	start_timer(&total_timer);
	if(PRINT_DBG_MSG) cout << "vmi_read_32_va_out()\n";
	vmi_instance_t vmi = vmi_map[string(input->vmi_key)];
	if(vmi==NULL || input->vmi_key == NULL ) {
		if (input->vmi_key == NULL) cerr << "NULL cannot be a valid vmi_key.\n";
		else if (vmi == NULL) cerr << "Unable to find the vmi keyed by " << input->vmi_key << " \n";
		output.server_status = 1;
		end_timer(&total_timer);
		return &output;
	}
	output.status = vmi_read_32_va( vmi, input->vaddr, input->pid, &(output.value));
	end_timer(&total_timer);
	return &output;
}
