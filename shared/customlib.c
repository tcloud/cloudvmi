#include "customlib.h"

void init_timer(TIMER_C *timer) {
	timer->total = 0;
	timer->has_started = 0;
}

void start_timer(TIMER_C *timer) {
	if(timer->has_started == 1) fprintf(stderr,"WARNING : The timer has already started - previous start point will be ignored.\n");
	timer->has_started = 1;
	clock_gettime(CLOCK_REALTIME, &(timer->start));
}

void end_timer(TIMER_C *timer) {
	struct timespec end;
	clock_gettime(CLOCK_REALTIME, &end);
	if (timer->has_started == 0) fprintf(stderr,"ERROR : Cannot call end_timer() - the timer has not started.\n");
	timer->total += (end.tv_sec - timer->start.tv_sec) + (end.tv_nsec - timer->start.tv_nsec) / BILLION;
	timer->has_started = 0;
}

double get_total_time(TIMER_C *timer) {
	return timer->total;
}
