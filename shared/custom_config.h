#ifndef __CUSTOM_CONFIG__
#define __CUSTOM_CONFIG__

#define PRINT_DBG_MSG 0
#define VLIBVMI_DEBUG
#define USE_LIBVMI_LIKE 1
#if USE_LIBVMI_LIKE
#define __USE_RPC__ 1
#define __USE_RESTFUL__ 0
#endif //end of USE_LIBVMI_LIKE
#endif //end of __CUSTOM_CONFIG__
