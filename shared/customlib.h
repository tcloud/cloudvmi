#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#ifndef _CUSTOM_LIB_
#define _CUSTOM_LIB_

#define BILLION  1E9;

typedef struct _TIMER_C {
	struct timespec start;
	double total;
	unsigned char has_started;
} TIMER_C;

void init_timer(TIMER_C *timer);
void start_timer(TIMER_C *timer);
void end_timer(TIMER_C *timer);
double get_total_time(TIMER_C *timer);

#endif //end of _CUSTOM_LIB_
