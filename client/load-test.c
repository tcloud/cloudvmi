#include "custom_config.h"

#if USE_LIBVMI_LIKE
#include "libvmi_like.h"
#else //Use original libvmi
#include <libvmi/libvmi.h>
#endif

#include "customlib.h"
#define _GNU_SOURCE
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <stdio.h>


#define PAGE_SIZE 1 << 12

int
main(
    int argc,
    char **argv)
{
	TIMER_C timer;
	TIMER_C *p_timer = &timer;
	init_timer(p_timer);

	vmi_instance_t vmi = NULL;

    uint32_t offset = 0;

    /* this is the VM or file that we are looking at */
    char *name = argv[1];
    unsigned long call_time = strtoul (argv[2],NULL,10);
#if __USE_RPC__
    if(argc == 4)
    	set_host_addr(argv[3]);
    reset_server_timers();
#endif
    /* initialize the libvmi library */
    status_t status;
    start_timer(p_timer);
    status = vmi_init(&vmi, VMI_AUTO | VMI_INIT_COMPLETE, name);
    end_timer(p_timer);
    if ( status == VMI_FAILURE ) {
        printf("Failed to init LibVMI library.\n");
    }

    int counter=0;
    while (counter < call_time) {
    	start_timer(p_timer);
    	vmi_get_memsize(vmi);
    	end_timer(p_timer);
        counter ++;
    }

    /* cleanup any memory associated with the libvmi instance */
	start_timer(p_timer);
    vmi_destroy(vmi);
    end_timer(p_timer);

    printf("Total number of calling : %d\n",counter + 2);
	printf("Total time for API calling : %lf\n",get_total_time(p_timer));
#if __USE_RPC__
	printf("Total time for rpc request() : %lf\n",get_total_time(&rpc_timer));
	print_result_remote();
#elif __USE_RESTFUL__
	printf("Total time for request() : %lf\n",get_total_time(&request_timer));
	printf("Total time for curl_perform() : %lf\n",get_total_time(&curl_timer));
#endif


    return 0;
}
