#include "custom_config.h"

#include "libvmi_like.h"

#include "customlib.h"
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <sys/mman.h>
#include <stdio.h>

int main(int argc, char**argv) {
	int i ;
	char* argv_part[3];
	printf("Kernel Modules in %s at %s\n", argv[1], argv[2]);
	argv_part[0] = argv[0];
	argv_part[1] = argv[1];
	argv_part[2] = argv[2];
	run (3,argv);
	printf("\n\n");
	printf("Kernel Modules %s at %s\n", argv[3], argv[4]);
	argv_part[0] = argv[0];
	argv_part[1] = argv[3];
	argv_part[2] = argv[4];
	run (3,argv);
}

int run( int argc, char **argv) {
	vmi_instance_t vmi;
	uint32_t offset;
	addr_t next_module, list_head;

	/* this is the VM or file that we are looking at */
	char *name = argv[1];

	set_host_addr(argv[2]);
	reset_server_timers();

	/* initialize the libvmi library */
	if (vmi_init(&vmi, VMI_AUTO | VMI_INIT_COMPLETE, name) ==
			VMI_FAILURE) {
		printf("Failed to init LibVMI library.\n");
		return 1;
	}

	/* pause the vm for consistent memory access */
	vmi_pause_vm(vmi);

	/* get the head of the module list */
	if (VMI_OS_LINUX == vmi_get_ostype(vmi)) {
		vmi_read_addr_ksym(vmi, "modules", &next_module);
	}
	else if (VMI_OS_WINDOWS == vmi_get_ostype(vmi)) {
		vmi_read_addr_ksym(vmi, "PsLoadedModuleList", &next_module);
	}
	list_head = next_module;

	/* walk the module list */
	while (1) {

		/* follow the next pointer */
		addr_t tmp_next = 0;

		vmi_read_addr_va(vmi, next_module, 0, &tmp_next);

		/* if we are back at the list head, we are done */
		if (list_head == tmp_next) {
			break;
		}

		/* print out the module name */

		/* Note: the module struct that we are looking at has a string
		 * directly following the next / prev pointers.  This is why you
		 * can just add the length of 2 address fields to get the name.
		 * See include/linux/module.h for mode details */
		if (VMI_OS_LINUX == vmi_get_ostype(vmi)) {
			char *modname = NULL;

			if (VMI_PM_IA32E == vmi_get_page_mode(vmi)) {   // 64-bit paging
				modname = vmi_read_str_va(vmi, next_module + 16, 0);
			}
			else {
				modname = vmi_read_str_va(vmi, next_module + 8, 0);
			}
			printf("%s\n", modname);
			free(modname);
		}
		else if (VMI_OS_WINDOWS == vmi_get_ostype(vmi)) {
			/*TODO don't use a hard-coded offsets here */
			/* this offset works with WinXP SP2 */
			unicode_string_t *us =
					vmi_read_unicode_str_va(vmi, next_module + 0x2c, 0);
			unicode_string_t out = { 0 };
			//         both of these work
			if (us &&
					VMI_SUCCESS == vmi_convert_str_encoding(us, &out,
							"UTF-8")) {
				printf("%s\n", out.contents);
				//            if (us &&
				//                VMI_SUCCESS == vmi_convert_string_encoding (us, &out, "WCHAR_T")) {
				//                printf ("%ls\n", out.contents);
				free(out.contents);
			}   // if
			if (us)
				vmi_free_unicode_str(us);
		}
		next_module = tmp_next;
	}

	error_exit:
	/* resume the vm */
	vmi_resume_vm(vmi);

	/* cleanup any memory associated with the libvmi instance */
	vmi_destroy(vmi);

	return 0;
}
