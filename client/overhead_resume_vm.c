#include "custom_config.h"

#if USE_LIBVMI_LIKE
#include "libvmi_like.h"
#else //Use original libvmi
#include <libvmi/libvmi.h>
#endif

#include "customlib.h"
#define _GNU_SOURCE
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <stdio.h>


#define PAGE_SIZE 1 << 12

int
main(
    int argc,
    char **argv)
{
	TIMER_C timer;
	init_timer(&timer);

	vmi_instance_t vmi = NULL;

    uint32_t offset = 0;

    /* this is the VM or file that we are looking at */
    char *name = argv[1];
    unsigned long call_time = strtoul (argv[2],NULL,10);
#if __USE_RPC__
    if(argc == 4)
    	set_host_addr(argv[3]);
#endif
    /* initialize the libvmi library */
    status_t status;
    status = vmi_init(&vmi, VMI_AUTO | VMI_INIT_COMPLETE, name);
    if ( status == VMI_FAILURE ) {
        printf("Failed to init LibVMI library.\n");
        goto error_exit;
    }

    int counter=0;
#if __USE_RPC__
    init_timer(&rpc_timer);
    reset_server_timers();
#endif
    while (counter < call_time) {
    	start_timer(&timer);
    	status = vmi_resume_vm(vmi);
    	end_timer(&timer);
        counter ++;
    }

error_exit:
    printf("Total number of calling : %d\n",counter);
	printf("Total time for API calling : %lf\n",get_total_time(&timer));
#if __USE_RPC__
	printf("Total time for rpc request() : %lf\n",get_total_time(&rpc_timer));
	print_result_remote();
#elif __USE_RESTFUL__
	printf("Total time for request() : %lf\n",get_total_time(&request_timer));
	printf("Total time for curl_perform() : %lf\n",get_total_time(&curl_timer));
#endif
    /* cleanup any memory associated with the libvmi instance */
    vmi_destroy(vmi);
    return 0;
}
