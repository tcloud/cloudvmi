/* -------------------------------------------------------------------
 * Use below options for compiling for USE_RESTFUL
 * -lcrypto
 * -ljansson
 * ------------------------------------------------------------------- */
#ifndef __LIBVMI_LIKE__
#define __LIBVMI_LIKE__
#include "custom_config.h"
#include "customlib.h"
#include "libvmi.h"
#include <stdlib.h>
#include <string.h>
#include <stdio.h>


#define MAX_VMI_INSTANCE_KEY_SIZE 20

#if __USE_RPC__
	#include "cloud_vmi.h"
	struct vmi_instance {
		char key[MAX_VMI_INSTANCE_KEY_SIZE+1] ;
	};
	extern TIMER_C rpc_timer;
	extern void set_host_addr(char* addr);
	extern void print_result_remote() ;
	extern void reset_server_timers() ;

//end of __USE_RPC__

#elif __USE_RESTFUL__
	#include <math.h>
	#include <jansson.h>
	#include <curl/curl.h>
	#define BUFFER_SIZE  (256 * 1024)  /* 256 KB */
	#define URL_FORMAT   "http://127.0.0.1:10102/%s"
	#define O_AUTH_PARAM "oauth_consumer_key=mon-vm&oauth_signature=mon-vm_mon-vm-secret"
	#define URL_SIZE     256
	#define POST_BUF_SIZE 1000
	struct vmi_instance {
		char key[MAX_VMI_INSTANCE_KEY_SIZE+1] ;
	};
	int b64_encode_string(const char *in, int in_len, char *out, int out_size);
	int b64_decode_string(const char *in, char *out, int out_size);
	extern TIMER_C request_timer, curl_timer;
#endif
//end of USE_RESTFUL
#endif
//end of __LIBVMI_LIKE__
