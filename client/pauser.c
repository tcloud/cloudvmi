#include "custom_config.h"
#include "libvmi_like.h"

#define _GNU_SOURCE
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <stdio.h>
int cnt = 0;
int
main(
    int argc,
    char **argv)
{
	vmi_instance_t vmi = NULL;
	printf("here %d\n",cnt++);
    char *name = argv[1];
    printf("here %d\n",cnt++);
    if (vmi_init(&vmi, VMI_AUTO | VMI_INIT_COMPLETE, name) ==
        VMI_FAILURE) {
        printf("Failed to init LibVMI library.\n");
        goto error_exit;
    }
    printf("here %d\n",cnt++);
    vmi_pause_vm(vmi);
    printf("here %d\n",cnt++);
error_exit:
	printf("here %d\n",cnt++);
    /* cleanup any memory associated with the libvmi instance */
    vmi_destroy(vmi);
    printf("here %d\n",cnt++);
    return 0;
}
