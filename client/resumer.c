#include "custom_config.h"
#include "libvmi_like.h"

#define _GNU_SOURCE
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <stdio.h>

int
main(
    int argc,
    char **argv)
{
	vmi_instance_t vmi = NULL;

    char *name = argv[1];

    if (vmi_init(&vmi, VMI_AUTO | VMI_INIT_COMPLETE, name) ==
        VMI_FAILURE) {
        printf("Failed to init LibVMI library.\n");
        goto error_exit;
    }

    vmi_resume_vm(vmi);

error_exit:

    /* cleanup any memory associated with the libvmi instance */
    vmi_destroy(vmi);

    return 0;
}
