//TODO Client 측 make all 했을 때 나오는 디버깅 메세지 수정하기
#include "libvmi_like.h"
#include <pwd.h>
#include <stdlib.h>
#include <sys/types.h>

#define __USE_RPC__ 1
#if __USE_RPC__

typedef void * (* remote_func) (void *argp, CLIENT *clnt) ;
TIMER_C rpc_timer;
static unsigned char is_first = 1;
void init_all_timers() {
	init_timer(&rpc_timer);
	is_first = 0;
}

static const char default_pf_addr[] = "127.0.0.1";

static char* host_addr = NULL;

void set_host_addr(char* addr) {
	host_addr = addr;
}
char* get_host_addr() {
	if (PRINT_DBG_MSG) printf("get_host_addr() <rpc> \n");
	if (host_addr == NULL) {
		return (char*)default_pf_addr;
	}
	return host_addr;
}
CLIENT * get_client() {
	static CLIENT * clnt = NULL;
	if (PRINT_DBG_MSG) printf("get_client() <rpc> \n");
	if (clnt == NULL) {
		clnt = clnt_create(get_host_addr(), CLOUD_VMI_PROG,CLOUD_VMI_VERS, "tcp");
		if (PRINT_DBG_MSG) printf("after clnt_create()\n");
		if (clnt == NULL) {
			clnt_pcreateerror(get_host_addr());
			exit(1);
		}
	}
	return clnt;
}
void reset_server_timers() {
	reset_server_timers_1(NULL, get_client());
	return;
}
void print_result_remote() {
	print_result_out* out;
	out = print_result_remote_1(NULL, get_client());
	if(out == NULL) return;
	if(out->server_status != 0) return;
	if((out->rtn)!=NULL) {
		printf("%s\n",out->rtn);
	}
	return;
}

/* This function is the pure code for vmi_init*/
status_t vmi_init_core(vmi_instance_t *vmi_p,uint32_t flags,char *name) {
	vmi_init_out *result ;
	vmi_init_in remote_arg;
	if (PRINT_DBG_MSG) printf("vmi_init() <rpc> \n");
	remote_arg.flags = flags;
	remote_arg.name = name;
	if(is_first) init_all_timers();
	start_timer(&rpc_timer);
	result = vmi_init_remote_1(&remote_arg, get_client());
	end_timer(&rpc_timer);
	if (result==NULL) {
		fprintf(stderr,"remote vmi result is null.\n");
		exit(1);
	}
	if (result->server_status != 0) {
		fprintf(stderr,"remote vmi server confronted an error.\n");
		exit(1);
	}
	if (result->status != VMI_SUCCESS) return (status_t)result->status;

	*vmi_p = (vmi_instance_t) malloc(sizeof(struct vmi_instance));
	memset(*vmi_p, 0, sizeof(struct vmi_instance));
	snprintf((*vmi_p)->key,MAX_VMI_INSTANCE_KEY_SIZE,"%s",result->vmi_key);
	return VMI_SUCCESS;
}

/* This function origins from core.c of libvmi.*/
static FILE * open_config_file( ){
	FILE *f = NULL;
	char location[100];
	char *sudo_user = NULL;
	struct passwd *pw_entry = NULL;

	/* first check home directory of sudo user */
	if ((sudo_user = getenv("SUDO_USER")) != NULL) {
		if ((pw_entry = getpwnam(sudo_user)) != NULL) {
			snprintf(location, 100, "%s/etc/libvmi.conf\0",
					pw_entry->pw_dir);
			//dbprint(VMI_DEBUG_CORE, "--looking for config file at %s\n", location);
			if ((f = fopen(location, "r")) != NULL) {
				goto success;
			}
		}
	}

	/* next check home directory for current user */
	snprintf(location, 100, "%s/etc/libvmi.conf\0", getenv("HOME"));
	//dbprint(VMI_DEBUG_CORE, "--looking for config file at %s\n", location);
	if ((f = fopen(location, "r")) != NULL) {
		goto success;
	}

	/* finally check in /etc */
	snprintf(location, 100, "/etc/libvmi.conf\0");
	//dbprint(VMI_DEBUG_CORE, "--looking for config file at %s\n", location);
	if ((f = fopen(location, "r")) != NULL) {
		goto success;
	}

	return NULL;
	success:
	//dbprint(VMI_DEBUG_CORE, "**Using config file at %s\n", location);
	return f;
}

/* */
status_t send_vm_info(){
	long f_size;
	char * conf_string;
	send_vm_info_out * result;
	send_vm_info_in remote_arg;
	size_t string_s, read_string_s;
	FILE * f = open_config_file();
	if (f == NULL) {
		printf("Cannot open the libvmi.conf file."); //TODO dbg print
		return VMI_FAILURE;
	}
	fseek (f, 0, SEEK_END);
	f_size = ftell(f);
	fseek (f, 0, SEEK_SET);
	string_s = sizeof(char) * f_size;
	conf_string = malloc(string_s);
	read_string_s = fread (conf_string, 1, f_size, f);
	fclose(f);
	if (read_string_s != f_size) {
		printf("Failed to read the file");
		free(conf_string);
		return VMI_FAILURE;
	}
	remote_arg.libvmi_conf = conf_string;

	//TODO remove later this printf function
	printf("libvmi.conf string : %s\n",conf_string);
	result = send_vm_info_1(&remote_arg, get_client());

	free(conf_string);
	if (result->server_status != 0) {
		fprintf(stderr,"remote vmi server confronted an error.\n");
		exit(1);
	}
	return (status_t)(result->rtn);
}


/* vmi_init_stable always send vm information first
 * and then ask the server to do vmi_init()
 */
status_t vmi_init_stable(vmi_instance_t *vmi_p,uint32_t flags,char *name) {
	status_t result;
	result = send_vm_info();
	if (result == VMI_FAILURE)
		return VMI_FAILURE;
	return vmi_init_core (vmi_p, flags, name);
}

/* By deafult, vmi_init first ask the server try vmi_init()
 * without vm info from the client.
 * If it fails it sends the file info and asks the server to retry
 * vmi_init()
 */
status_t vmi_init(vmi_instance_t *vmi_p,uint32_t flags,char *name) {
	status_t result;
	result = vmi_init_core(vmi_p,flags,name);
	if (result == VMI_SUCCESS)
		return VMI_SUCCESS;
	return vmi_init_stable(vmi_p,flags,name);
}

status_t vmi_init_custom(
		vmi_instance_t *vmi,
		uint32_t flags,
		vmi_config_t config){
	return 0;
}
status_t vmi_init_complete(
		vmi_instance_t *vmi,
		char *config){
	return 0;
}
status_t vmi_init_complete_custom (vmi_instance_t *vmi, uint32_t flags, vmi_config_t config) {
	return 0;
}

status_t vmi_destroy(vmi_instance_t vmi) {
	vmi_destroy_out* result;
	vmi_destroy_in remote_arg;
	if (PRINT_DBG_MSG) printf("vmi_destroy() <rpc> \n");
	if(vmi == NULL)
		return VMI_FAILURE;
	remote_arg.vmi_key = vmi->key;
	if(is_first) init_all_timers();
	start_timer(&rpc_timer);
	result = vmi_destroy_remote_1(&remote_arg, get_client());
	end_timer(&rpc_timer);
	if (result==NULL) {
		fprintf(stderr,"remote vmi result is null.\n");
		exit(1);
	}
	if (result->server_status != 0) {
		fprintf(stderr,"remote vmi server confronted an error.\n");
		exit(1);
	}
	return (status_t)(result->rtn);
}

addr_t vmi_translate_kv2p(
		vmi_instance_t vmi,
		addr_t vaddr){
	return 0;
}

addr_t vmi_translate_uv2p(
		vmi_instance_t vmi,
		addr_t vaddr,
		int pid){
	return 0;
}

addr_t vmi_translate_ksym2v( vmi_instance_t vmi, char *symbol) {
	vmi_translate_ksym2v_out *result;
	vmi_translate_ksym2v_in remote_arg;
	if (PRINT_DBG_MSG) printf("vmi_translate_ksym2v() <rpc> \n");
	if(vmi == NULL || symbol == NULL) {
		if(vmi == NULL) fprintf(stderr,"vmi is NULL\n");
		if(symbol == NULL) fprintf(stderr,"symbol is NULL\n");
		return 0;
	}
	remote_arg.vmi_key = vmi->key;
	remote_arg.sym = symbol;
	if(is_first) init_all_timers();
	start_timer(&rpc_timer);
	result = vmi_translate_ksym2v_remote_1(&remote_arg, get_client());
	end_timer(&rpc_timer);
	if (result==NULL) {
		fprintf(stderr,"remote vmi result is null.\n");
		exit(1);
	}
	if (result->server_status != 0) {
		fprintf(stderr,"remote vmi server confronted an error.\n");
		exit(1);
	}
	return (addr_t)(result->rtn);
}

addr_t vmi_pid_to_dtb(
		vmi_instance_t vmi,
		int pid){
	return 0;
}


addr_t vmi_pagetable_lookup (
		vmi_instance_t vmi,
		addr_t dtb,
		addr_t vaddr){
	return 0;
}

size_t vmi_read_ksym(
		vmi_instance_t vmi,
		char *sym,
		void *buf,
		size_t count){
	return 0;
}

size_t vmi_read_va(
		vmi_instance_t vmi,
		addr_t vaddr,
		int pid,
		void *buf,
		size_t count){
	return 0;
}

size_t vmi_read_pa( vmi_instance_t vmi, addr_t paddr, void *buf, size_t count) {
	return 0;
}

status_t vmi_read_8_ksym(
		vmi_instance_t vmi,
		char *sym,
		uint8_t * value){
	return 0;
}

status_t vmi_read_16_ksym(
		vmi_instance_t vmi,
		char *sym,
		uint16_t * value){
	return 0;
}
status_t vmi_read_32_ksym(
		vmi_instance_t vmi,
		char *sym,
		uint32_t * value){
	return 0;
}

status_t vmi_read_64_ksym(
		vmi_instance_t vmi,
		char *sym,
		uint64_t * value){
	return 0;
}

status_t vmi_read_addr_ksym(vmi_instance_t vmi, char *sym, addr_t *value) {
	vmi_read_addr_ksym_out* result;
	vmi_read_addr_ksym_in remote_arg;
	if (PRINT_DBG_MSG) printf("vmi_read_addr_ksym() <rpc> \n");
	if(vmi == NULL || sym == NULL) {
		if (vmi == NULL) fprintf(stderr,"vmi_instance_t should not be a NULL!\n");
		if (sym == NULL) fprintf(stderr,"Argument sym should not be a NULL!\n");
		return VMI_FAILURE;
	}
	remote_arg.vmi_key = vmi->key;
	remote_arg.sym = sym;
	if(is_first) init_all_timers();
	start_timer(&rpc_timer);
	result = vmi_read_addr_ksym_remote_1(&remote_arg, get_client());
	end_timer(&rpc_timer);
	if (result==NULL) {
		fprintf(stderr,"remote vmi result is null.\n");
		exit(1);
	}
	if (result->server_status != 0) {
		fprintf(stderr,"remote vmi server confronted an error.\n");
		exit(1);
	}
	*value = result->value;
	return (status_t)(result->status);
}

char *vmi_read_str_ksym(
		vmi_instance_t vmi,
		char *sym){
	return 0;
}

status_t vmi_read_8_va(
		vmi_instance_t vmi,
		addr_t vaddr,
		int pid,
		uint8_t * value){
	return 0;
}

status_t vmi_read_16_va(
		vmi_instance_t vmi,
		addr_t vaddr,
		int pid,
		uint16_t * value){
	return 0;
}

status_t vmi_read_32_va( vmi_instance_t vmi, addr_t vaddr, int pid, uint32_t * value) {
	vmi_read_32_va_out *result;
	vmi_read_32_va_in remote_arg;
	if (PRINT_DBG_MSG) printf("vmi_read_32_va() <rpc> \n");
	if(vmi == NULL || value == NULL) {
		if(vmi == NULL) fprintf(stderr,"vmi is NULL\n");
		if(value == NULL) fprintf(stderr,"output parameter value is NULL.\n");
		return -1;
	}
	remote_arg.vmi_key = vmi->key;
	if(is_first) init_all_timers();
	start_timer(&rpc_timer);
	result = vmi_read_32_va_remote_1(&remote_arg, get_client());
	end_timer(&rpc_timer);
	if (result==NULL) {
		fprintf(stderr,"remote vmi result is null.\n");
		exit(1);
	}
	if (result->server_status != 0) {
		fprintf(stderr,"remote vmi server confronted an error.\n");
		exit(1);
	}
	*value = result->value;
	return (status_t)(result->status);
}

status_t vmi_read_64_va(
		vmi_instance_t vmi,
		addr_t vaddr,
		int pid,
		uint64_t * value){
	return 0;
}

status_t vmi_read_addr_va (vmi_instance_t vmi, addr_t vaddr, int pid, addr_t *value) {
	vmi_read_addr_va_out* result;
	vmi_read_addr_va_in remote_arg;
	if (PRINT_DBG_MSG) printf("vmi_read_addr_va() <rpc> \n");
	if(vmi == NULL) {
		if (vmi == NULL) fprintf(stderr,"vmi_instance_t should not be a NULL!\n");
		return VMI_FAILURE;
	}
	remote_arg.vmi_key = vmi->key;
	remote_arg.vaddr = vaddr;
	remote_arg.pid = pid;
	if(is_first) init_all_timers();
	start_timer(&rpc_timer);
	result = vmi_read_addr_va_remote_1(&remote_arg, get_client());
	end_timer(&rpc_timer);
	if (result==NULL) {
		fprintf(stderr,"remote vmi result is null.\n");
		exit(1);
	}
	if (result->server_status != 0) {
		fprintf(stderr,"remote vmi server confronted an error.\n");
		exit(1);
	}
	*value = result->value;
	return (status_t)(result->status);
}

char *vmi_read_str_va(vmi_instance_t vmi, addr_t vaddr, int pid) {
	vmi_read_str_va_out *result;
	vmi_read_str_va_in remote_arg;
	if (PRINT_DBG_MSG) printf("vmi_read_str_va() <rpc> \n");
	if(vmi == NULL) {
		fprintf(stderr,"VMI is NULL\n");
		return NULL;
	}
	remote_arg.vmi_key = vmi->key;
	remote_arg.vaddr = vaddr;
	remote_arg.pid = pid;
	if(is_first) init_all_timers();
	start_timer(&rpc_timer);
	result = vmi_read_str_va_remote_1(&remote_arg, get_client());
	end_timer(&rpc_timer);
	if (result==NULL) {
		fprintf(stderr,"remote vmi result is null.\n");
		exit(1);
	}
	if (result->server_status != 0) {
		fprintf(stderr,"remote vmi server confronted an error.\n");
		exit(1);
	}
	return result->rtn;
}

unicode_string_t *vmi_read_unicode_str_va(vmi_instance_t vmi, addr_t vaddr, int pid) {
	vmi_read_unicode_str_va_out *result;
	vmi_read_unicode_str_va_in remote_arg;
	if (PRINT_DBG_MSG) printf("vmi_read_unicode_str_va() <rpc> \n");
	if(vmi == NULL){
		fprintf(stderr,"VMI is NULL\n");
		return NULL;
	}
	remote_arg.vmi_key = vmi->key;
	remote_arg.vaddr = vaddr;
	remote_arg.pid = pid;
	if(is_first) init_all_timers();
	start_timer(&rpc_timer);
	result = vmi_read_unicode_str_va_remote_1(&remote_arg, get_client());
	end_timer(&rpc_timer);
	if (result==NULL) {
		fprintf(stderr,"remote vmi result is null.\n");
		exit(1);
	}
	if (result->server_status != 0) {
		fprintf(stderr,"remote vmi server confronted an error.\n");
		exit(1);
	}
	if (result->encoding == NULL) {
		fprintf(stderr,"Returned Encoding type is NULL!\n");
		exit(1);
	}
	if (result->contents.contents_val == NULL || result->contents.contents_len <= 0) {
		return NULL;
	}
	unicode_string_t *us = malloc(sizeof(unicode_string_t));
	us->length = result->contents.contents_len;
	us->contents = malloc(us->length);
	char* temp_str = malloc(strlen(result->encoding)+1);
	memcpy(us->contents, result->contents.contents_val, us->length);
	snprintf(temp_str, strlen(result->encoding)+1,"%s", result->encoding);
	us->encoding = (const char*)temp_str;
	return us;
}


status_t vmi_convert_str_encoding( //TODO change this to non-RPC local function
		const unicode_string_t *in,
		unicode_string_t *out,
		const char *outencoding){

	vmi_convert_str_encoding_out *result;
	vmi_convert_str_encoding_in remote_arg;
	if (PRINT_DBG_MSG) printf("vmi_convert_str_encoding() <rpc> \n");
	if (in == NULL || in->contents == NULL || in->encoding == NULL
			|| in->length <= 0 || out == NULL || outencoding == NULL) {
		fprintf(stderr,"Wrong Argumetns.");
		return VMI_FAILURE;
	}
	remote_arg.in_encoding = malloc(strlen(in->encoding)+1);
	snprintf(remote_arg.in_encoding, strlen(in->encoding)+1, "%s",in->encoding);
	remote_arg.out_encoding = malloc(strlen(outencoding)+1);
	snprintf(remote_arg.in_encoding, strlen(outencoding)+1, "%s",outencoding);
	remote_arg.contents.contents_len = in->length;
	remote_arg.contents.contents_val = in->contents;
	if(is_first) init_all_timers();
	start_timer(&rpc_timer);
	result = vmi_convert_str_encoding_remote_1(&remote_arg, get_client());
	end_timer(&rpc_timer);
	if (result==NULL) {
		fprintf(stderr,"remote vmi result is null.\n");
		exit(1);
	}
	if (result->server_status != 0) {
		fprintf(stderr,"remote vmi server confronted an error.\n");
		exit(1);
	}
	unicode_string_t *us = malloc(sizeof(unicode_string_t));
	us->length = result->contents.contents_len;
	us->contents = malloc(us->length);
	char* temp_str = malloc(strlen(result->encoding)+1);
	memcpy(us->contents, result->contents.contents_val, us->length);
	snprintf(temp_str, strlen(result->encoding)+1, "%s", result->encoding);
	us->encoding = (const char*)temp_str;
	out = us;
	return result->status;
}

void vmi_free_unicode_str(
		unicode_string_t *p_us){
	free(p_us->contents);
	free((char*)p_us->encoding);
	free(p_us);
}

status_t vmi_read_8_pa(
		vmi_instance_t vmi,
		addr_t paddr,
		uint8_t * value){
	return 0;
}

status_t vmi_read_16_pa(
		vmi_instance_t vmi,
		addr_t paddr,
		uint16_t * value){
	return 0;
}

status_t vmi_read_32_pa(
		vmi_instance_t vmi,
		addr_t paddr,
		uint32_t * value){
	return 0;
}

status_t vmi_read_64_pa(
		vmi_instance_t vmi,
		addr_t paddr,
		uint64_t * value){
	return 0;
}

status_t vmi_read_addr_pa(
		vmi_instance_t vmi,
		addr_t paddr,
		addr_t *value){
	return 0;
}

char *vmi_read_str_pa(
		vmi_instance_t vmi,
		addr_t paddr){
	return NULL;
}

size_t vmi_write_ksym(
		vmi_instance_t vmi,
		char *sym,
		void *buf,
		size_t count){
	return 0;
}

size_t vmi_write_va(
		vmi_instance_t vmi,
		addr_t vaddr,
		int pid,
		void *buf,
		size_t count){
	return 0;
}

size_t vmi_write_pa(
		vmi_instance_t vmi,
		addr_t paddr,
		void *buf,
		size_t count){
	return 0;
}

status_t vmi_write_8_ksym(
		vmi_instance_t vmi,
		char *sym,
		uint8_t * value){
	return 0;
}

status_t vmi_write_16_ksym(
		vmi_instance_t vmi,
		char *sym,
		uint16_t * value){
	return 0;
}

status_t vmi_write_32_ksym(
		vmi_instance_t vmi,
		char *sym,
		uint32_t * value){
	return 0;
}

status_t vmi_write_64_ksym(
		vmi_instance_t vmi,
		char *sym,
		uint64_t * value){
	return 0;
}

status_t vmi_write_8_va(
		vmi_instance_t vmi,
		addr_t vaddr,
		int pid,
		uint8_t * value){
	return 0;
}

status_t vmi_write_16_va(
		vmi_instance_t vmi,
		addr_t vaddr,
		int pid,
		uint16_t * value){
	return 0;
}

status_t vmi_write_32_va(
		vmi_instance_t vmi,
		addr_t vaddr,
		int pid,
		uint32_t * value){
	return 0;
}

status_t vmi_write_64_va(
		vmi_instance_t vmi,
		addr_t vaddr,
		int pid,
		uint64_t * value){
	return 0;
}

status_t vmi_write_8_pa(
		vmi_instance_t vmi,
		addr_t paddr,
		uint8_t * value){
	return 0;
}

status_t vmi_write_16_pa(
		vmi_instance_t vmi,
		addr_t paddr,
		uint16_t * value){
	return 0;
}

status_t vmi_write_32_pa(
		vmi_instance_t vmi,
		addr_t paddr,
		uint32_t * value){
	return 0;
}

status_t vmi_write_64_pa(
		vmi_instance_t vmi,
		addr_t paddr,
		uint64_t * value){
	return 0;
}

void vmi_print_hex(
		unsigned char *data,
		unsigned long length){
	return;
}

void vmi_print_hex_ksym(
		vmi_instance_t vmi,
		char *sym,
		size_t length){
	return;
}

void vmi_print_hex_va(
		vmi_instance_t vmi,
		addr_t vaddr,
		int pid,
		size_t length){
	return;
}

void vmi_print_hex_pa(
		vmi_instance_t vmi,
		addr_t paddr,
		size_t length){
	return;
}

char *vmi_get_name( vmi_instance_t vmi) {
	vmi_get_name_out *result;
	vmi_get_name_in remote_arg;
	if (PRINT_DBG_MSG) printf("vmi_get_name() <rpc> \n");
	if(vmi == NULL){
		fprintf(stderr,"VMI is NULL\n");
		return NULL;
	}
	remote_arg.vmi_key = vmi->key;
	if(is_first) init_all_timers();
	start_timer(&rpc_timer);
	result = vmi_get_name_remote_1(&remote_arg, get_client());
	end_timer(&rpc_timer);
	if (result==NULL) {
		fprintf(stderr,"remote vmi result is null.\n");
		exit(1);
	}
	if (result->server_status != 0) {
		fprintf(stderr,"remote vmi server confronted an error.\n");
		exit(1);
	}
	return result->rtn;
}

unsigned long vmi_get_vmid(vmi_instance_t vmi) {
	vmi_get_vmid_out *result;
	vmi_get_vmid_in remote_arg;
	if (PRINT_DBG_MSG) printf("vmi_get_vmid() <rpc> \n");
	if(vmi == NULL)
		return -1;
	remote_arg.vmi_key = vmi->key;
	if(is_first) init_all_timers();
	start_timer(&rpc_timer);
	result = vmi_get_vmid_remote_1(&remote_arg, get_client());
	end_timer(&rpc_timer);
	if (result==NULL) {
		fprintf(stderr,"remote vmi result is null.\n");
		exit(1);
	}
	if (result->server_status != 0) {
		fprintf(stderr,"remote vmi server confronted an error.\n");
		exit(1);
	}
	return (unsigned long)(result->rtn);
}

uint32_t vmi_get_access_mode( vmi_instance_t vmi ) {
	vmi_get_access_mode_out *result;
	vmi_get_access_mode_in remote_arg;
	if (PRINT_DBG_MSG) printf("vmi_get_access_mode() <rpc> \n");
	if(vmi == NULL)
		return -1;
	remote_arg.vmi_key = vmi->key;
	if(is_first) init_all_timers();
	start_timer(&rpc_timer);
	result = vmi_get_access_mode_remote_1(&remote_arg, get_client());
	end_timer(&rpc_timer);
	if (result==NULL) {
		fprintf(stderr,"remote vmi result is null.\n");
		exit(1);
	}
	if (result->server_status != 0) {
		fprintf(stderr,"remote vmi server confronted an error.\n");
		exit(1);
	}
	return result->rtn;
}

page_mode_t vmi_get_page_mode( vmi_instance_t vmi ) {
	vmi_get_page_mode_out *result;
	vmi_get_page_mode_in remote_arg;
	if (PRINT_DBG_MSG) printf("vmi_get_page_mode() <rpc> \n");
	if(vmi == NULL)
		return -1;
	remote_arg.vmi_key = vmi->key;
	if(is_first) init_all_timers();
	start_timer(&rpc_timer);
	result = vmi_get_page_mode_remote_1(&remote_arg, get_client());
	end_timer(&rpc_timer);
	if (result==NULL) {
		fprintf(stderr,"remote vmi result is null.\n");
		exit(1);
	}
	if (result->server_status != 0) {
		fprintf(stderr,"remote vmi server confronted an error.\n");
		exit(1);
	}
	return (page_mode_t)(result->rtn);
}

os_t vmi_get_ostype(vmi_instance_t vmi) {
	vmi_get_ostype_out *result;
	vmi_get_ostype_in remote_arg;
	if (PRINT_DBG_MSG) printf("vmi_get_ostype() <rpc> \n");
	if(vmi == NULL)
		return -1;
	remote_arg.vmi_key = vmi->key;
	if(is_first) init_all_timers();
	start_timer(&rpc_timer);
	result = vmi_get_ostype_remote_1(&remote_arg, get_client());
	end_timer(&rpc_timer);
	if (result==NULL) {
		fprintf(stderr,"remote vmi result is null.\n");
		exit(1);
	}
	if (result->server_status != 0) {
		fprintf(stderr,"remote vmi server confronted an error.\n");
		exit(1);
	}
	return (os_t)(result->rtn);
}

win_ver_t vmi_get_winver(
		vmi_instance_t vmi){
	return 0;
}

const char *vmi_get_winver_str(
		vmi_instance_t vmi){
	return 0;
}

win_ver_t vmi_get_winver_manual(
		vmi_instance_t vmi,
		addr_t kdvb_pa){
	return 0;
}

unsigned long vmi_get_offset( vmi_instance_t vmi, char *offset_name) {
	vmi_get_offset_out *result;
	vmi_get_offset_in remote_arg;
	if (PRINT_DBG_MSG) printf("vmi_get_offset() <rpc> \n");
	//함수 parameter validity 체크할것.
	if(vmi == NULL) {
		fprintf(stderr,"vmi_instance_t is NULL\n");
		return 0;
	}
	if(offset_name == NULL) {
		fprintf(stderr,"offset name is NULL\n");
		return 0;
	}
	remote_arg.vmi_key = vmi->key;
	remote_arg.offset_name = offset_name;
	if(is_first) init_all_timers();
	start_timer(&rpc_timer);
	result = vmi_get_offset_remote_1(&remote_arg, get_client());
	end_timer(&rpc_timer);
	if (result==NULL) {
		fprintf(stderr,"remote vmi result is null.\n");
		exit(1);
	}
	if (result->server_status != 0) {
		fprintf(stderr,"remote vmi server confronted an error.\n");
		exit(1);
	}
	return (unsigned int) result->rtn;
}

unsigned long vmi_get_memsize( vmi_instance_t vmi) {
	vmi_get_memsize_out *result;
	vmi_get_memsize_in remote_arg;
	if (PRINT_DBG_MSG) printf("vmi_get_memsize() <rpc> \n");
	if(vmi == NULL)
		return VMI_FAILURE;
	remote_arg.vmi_key = vmi->key;
	if(is_first) init_all_timers();
	start_timer(&rpc_timer);
	result = vmi_get_memsize_remote_1(&remote_arg, get_client());
	end_timer(&rpc_timer);
	if (result==NULL) {
		fprintf(stderr,"remote vmi result is null.\n");
		exit(1);
	}
	if (result->server_status != 0) {
		fprintf(stderr,"remote vmi server confronted an error.\n");
		exit(1);
	}
	return (unsigned long)(result->rtn);
}

unsigned int vmi_get_num_vcpus (
		vmi_instance_t vmi){
	return 0;
}

status_t vmi_get_vcpureg(
		vmi_instance_t vmi,
		reg_t *value,
		registers_t reg,
		unsigned long vcpu){
	return 0;
}

status_t vmi_pause_vm(vmi_instance_t vmi){
	vmi_pause_vm_out* result;
	vmi_pause_vm_in remote_arg;
	if (PRINT_DBG_MSG) printf("vmi_pause_vm() <rpc> \n");
	if(vmi == NULL)
		return VMI_FAILURE;
	remote_arg.vmi_key = vmi->key;
	if(is_first) init_all_timers();
	start_timer(&rpc_timer);
	result = vmi_pause_vm_remote_1(&remote_arg, get_client());
	end_timer(&rpc_timer);
	if (result==NULL) {
		fprintf(stderr,"remote vmi result is null.\n");
		exit(1);
	}
	if (result->server_status != 0) {
		fprintf(stderr,"remote vmi server confronted an error.\n");
		exit(1);
	}
	return (status_t)(result->rtn);
}

status_t vmi_resume_vm(vmi_instance_t vmi) {
	vmi_resume_vm_out* result;
	vmi_resume_vm_in remote_arg;
	if (PRINT_DBG_MSG) printf("vmi_resume_vm() <rpc>\n");
	if(vmi == NULL)
		return VMI_FAILURE;
	remote_arg.vmi_key = vmi->key;
	if(is_first) init_all_timers();
	start_timer(&rpc_timer);
	result = vmi_resume_vm_remote_1(&remote_arg, get_client());
	end_timer(&rpc_timer);
	if (result==NULL) {
		fprintf(stderr,"remote vmi result is null.\n");
		exit(1);
	}
	if (result->server_status != 0) {
		fprintf(stderr,"remote vmi server confronted an error.\n");
		exit(1);
	}
	return (status_t)(result->rtn);
}

void vmi_v2pcache_flush(
		vmi_instance_t vmi){
	return;
}

void vmi_v2pcache_add(
		vmi_instance_t vmi,
		addr_t va,
		addr_t dtb,
		addr_t pa){
	return;
}

void vmi_symcache_flush(
		vmi_instance_t vmi){
	return;
}

void vmi_symcache_add(
		vmi_instance_t vmi,
		char *sym,
		addr_t va){
	return;
}

void vmi_pidcache_flush(
		vmi_instance_t vmi){
	return;
}

void vmi_pidcache_add(
		vmi_instance_t vmi,
		int pid,
		addr_t dtb){
	return;
}

status_t vmi_handle_event(
		vmi_instance_t vmi,
		vmi_event_t event,
		event_callback_t callback){
	return 0;
}
status_t vmi_clear_event(
		vmi_instance_t vmi,
		vmi_event_t event){
	return 0;
}

status_t vmi_events_listen(
		vmi_instance_t vmi,
		uint32_t timeout){
	return 0;
}
//end of __USE_RPC__
#elif __USE_RESTFUL__

/* Return the offset of the first newline in text or the length of
   text if there's no newline */
struct write_result {
	char *data;
	int pos;
};

static size_t write_response(void *ptr, size_t size, size_t nmemb, void *stream) {
	struct write_result *result = (struct write_result *)stream;

	if(result->pos + size * nmemb >= BUFFER_SIZE - 1)
	{
		fprintf(stderr, "error: too small buffer\n");
		return 0;
	}

	memcpy(result->data + result->pos, ptr, size * nmemb);
	result->pos += size * nmemb;

	return size * nmemb;
}
TIMER_C request_timer, curl_timer;
static unsigned char is_first = 1;
static CURL *curl;
static void init_all_timers() {
	init_timer(&request_timer);
	init_timer(&curl_timer);
	is_first = 0;
}
static char *request(const char *url, char *postData) {
	if(is_first) init_all_timers();
	start_timer(&request_timer);
	CURLcode status;
	char *data;
	long code;
	if(curl == NULL) {
		printf("curl is null\n");
		curl = curl_easy_init();
	}

	data = malloc(BUFFER_SIZE);
	if(!curl || !data) {
		end_timer(&request_timer);
		return NULL;
	}

	struct write_result write_result = {
			.data = data,
			.pos = 0
	};

	curl_easy_setopt(curl, CURLOPT_URL, url);
	curl_easy_setopt(curl, CURLOPT_POSTFIELDS, postData);
	curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_response);
	curl_easy_setopt(curl, CURLOPT_WRITEDATA, &write_result);

	start_timer(&curl_timer);
	status = curl_easy_perform(curl);
	end_timer(&curl_timer);
	if(status != 0)
	{
		fprintf(stderr, "error: unable to request data from %s:\n", url);
		fprintf(stderr, "%s\n", curl_easy_strerror(status));
		end_timer(&request_timer);
		return NULL;
	}

	curl_easy_getinfo(curl, CURLINFO_RESPONSE_CODE, &code);
	if(code != 200)
	{
		fprintf(stderr, "error: server responded with code %ld\n", code);
		end_timer(&request_timer);
		return NULL;
	}

	//    curl_easy_cleanup(curl);
	//    curl_global_cleanup();

	/* zero-terminate the result */
	data[write_result.pos] = '\0';

	end_timer(&request_timer);
	return data;
}

// This function is by vmi_destroy()
void cleanup_curl() {
	if(curl!=NULL)
		curl_easy_cleanup(curl);
	curl = NULL;
	curl_global_cleanup();
}

//TODO
status_t vmi_init(vmi_instance_t *vmi_p,uint32_t flags,char *name) {
	if (PRINT_DBG_MSG) printf("vmi_init()\n");
	size_t i;
	char *text;
	char url[URL_SIZE];
	char post_data[POST_BUF_SIZE];
	char *page = "vmi_init";

	json_t *root;
	json_t *rtn;
	json_t *vmi_key;
	json_error_t error;
	const char *message_text;

	snprintf(url, URL_SIZE, URL_FORMAT, page);
	snprintf(post_data, POST_BUF_SIZE, "%s&target_vm_name=%s&flags=%lu", O_AUTH_PARAM, name, (unsigned long)flags);

	text = request(url,post_data);
	if (!text)
		return VMI_FAILURE;

	root = json_loads(text, 0, &error);
	if (PRINT_DBG_MSG) printf("return text\n%s\n",text);
	free(text);

	if (!root) {
		fprintf(stderr, "error: on line %d: %s\n", error.line, error.text);
		return VMI_FAILURE;
	}
	/*if(PRINT_DBG_MSG) {
        char* dump_str = json_dumps(root,JSON_COMPACT);
        printf("root is :\n%s\n",dump_str);
        free(dump_str);
    }*/
	rtn = json_object_get(root, "rtn");
	if (!json_is_integer(rtn)) {
		fprintf(stderr, "error: rtn is not an integer\n");
		json_decref(root);
		return VMI_FAILURE;
	}
	if (json_integer_value(rtn)!=VMI_SUCCESS) {
		json_decref(root);
		return VMI_FAILURE;
	}
	/*if(PRINT_DBG_MSG) {
        char* dump_str = json_dumps(rtn,JSON_COMPACT);
        printf("rtn is :\n%s\n",dump_str);
        free(dump_str);
    }*/

	vmi_key = json_object_get(root, "vmi_key");
	if (!json_is_string(vmi_key)) {
		fprintf(stderr, "error: vmi_key is not a string\n");
		json_decref(root);
		return VMI_FAILURE;
	}
	/*if(PRINT_DBG_MSG) {
        char* dump_str = json_dumps(vmi_key,JSON_COMPACT);
        printf("vmi_key is :\n%s\n",dump_str);
        free(dump_str);
    }*/

	*vmi_p = (vmi_instance_t) malloc(sizeof(struct vmi_instance));
	memset(*vmi_p, 0, sizeof(struct vmi_instance));
	snprintf((*vmi_p)->key,MAX_VMI_INSTANCE_KEY_SIZE,"%s",json_string_value(vmi_key));
	json_decref(root);
	return VMI_SUCCESS;
}

status_t vmi_init_custom(
		vmi_instance_t *vmi,
		uint32_t flags,
		vmi_config_t config){
	return 0;
}
status_t vmi_init_complete(
		vmi_instance_t *vmi,
		char *config){
	return 0;
}
status_t vmi_init_complete_custom (vmi_instance_t *vmi, uint32_t flags, vmi_config_t config) {
	return 0;
}

//TODO
status_t vmi_destroy(vmi_instance_t vmi) {
	if (PRINT_DBG_MSG) printf("vmi_destroy()\n");
	size_t i;
	char *text;
	char url[URL_SIZE];
	char post_data[POST_BUF_SIZE];
	char *page = "vmi_destroy";

	json_t *root;
	json_t *rtn;
	json_error_t error;
	const char *message_text;

	snprintf(url, URL_SIZE, URL_FORMAT, page);
	snprintf(post_data, POST_BUF_SIZE, "%s&vmi_key=%s",O_AUTH_PARAM,vmi->key);

	text = request(url,post_data);
	if (!text)
		return VMI_FAILURE;

	root = json_loads(text, 0, &error);
	if (PRINT_DBG_MSG) printf("return text\n%s\n",text);
	free(text);

	if (!root) {
		fprintf(stderr, "error: on line %d: %s\n", error.line, error.text);
		return VMI_FAILURE;
	}

	rtn = json_object_get(root, "rtn");
	if (!json_is_integer(rtn)) {
		fprintf(stderr, "error: rtn is not an integer\n");
		json_decref(root);
		return VMI_FAILURE;
	}
	if (json_integer_value(rtn)!=VMI_SUCCESS) {
		json_decref(root);
		return VMI_FAILURE;
	}
	json_decref(root);
	free(vmi);
	cleanup_curl();
	return VMI_SUCCESS;
}

addr_t vmi_translate_kv2p(
		vmi_instance_t vmi,
		addr_t vaddr){
	return 0;
}

addr_t vmi_translate_uv2p(
		vmi_instance_t vmi,
		addr_t vaddr,
		int pid){
	return 0;
}

//TODO
addr_t vmi_translate_ksym2v( vmi_instance_t vmi, char *symbol) {
	if (PRINT_DBG_MSG) printf("vmi_translate_ksym2v()\n");
	size_t i;
	char *text;
	char url[URL_SIZE];
	char post_data[POST_BUF_SIZE];
	char *page = "vmi_translate_ksym2v";

	json_t *root;
	json_t *j_rtn;
	json_error_t error;
	const char *message_text;

	snprintf(url, URL_SIZE, URL_FORMAT, page);
	snprintf(post_data, POST_BUF_SIZE, "%s&vmi_key=%s&symbol=%s",O_AUTH_PARAM,vmi->key,symbol);

	text = request(url,post_data);
	if (!text) {
		fprintf(stderr, "error: fail to request %s()\n",page);
		return 0;
	}

	root = json_loads(text, 0, &error);
	if (PRINT_DBG_MSG) printf("return text\n%s\n",text);
	free(text);

	if (!root) {
		fprintf(stderr, "error: on line %d: %s\n", error.line, error.text);
		return 0;
	}

	j_rtn = json_object_get(root, "rtn");
	if (!json_is_string(j_rtn)) {
		fprintf(stderr, "error: rtn is not a string\n");
		json_decref(root);
		return 0;
	}
	char* endptr;
	addr_t rtn = strtoull(json_string_value(j_rtn),&endptr,10);
	if(PRINT_DBG_MSG) printf("endptr is %s",endptr);
	json_decref(root);
	return rtn;
}

addr_t vmi_pid_to_dtb(
		vmi_instance_t vmi,
		int pid){
	return 0;
}


addr_t vmi_pagetable_lookup (
		vmi_instance_t vmi,
		addr_t dtb,
		addr_t vaddr){
	return 0;
}

size_t vmi_read_ksym(
		vmi_instance_t vmi,
		char *sym,
		void *buf,
		size_t count){
	return 0;
}

size_t vmi_read_va(
		vmi_instance_t vmi,
		addr_t vaddr,
		int pid,
		void *buf,
		size_t count){
	return 0;
}

//TODO : this function is using the function b64_decode_string().
//The problem is b64_decode_string() is strangely appending some garbage string
//at the end of the decoded string and it is also reflected its return value.
//At this moment, this problem is treating by truncating the decoded string
//based on the received 'rtn' value from server.
size_t vmi_read_pa( vmi_instance_t vmi, addr_t paddr, void *buf, size_t count) {
	static int call_counter = 0;
	if (PRINT_DBG_MSG) printf("vmi_read_pa()\n");
	size_t i;
	char *text;
	char url[URL_SIZE];
	char post_data[POST_BUF_SIZE];
	char *page = "vmi_read_pa";

	json_t *root;
	json_t *j_rtn;
	json_t *j_buf;
	json_error_t error;

	snprintf(url, URL_SIZE, URL_FORMAT, page);
	snprintf(post_data, POST_BUF_SIZE, "%s&vmi_key=%s&paddr=%llu&count=%u",O_AUTH_PARAM,vmi->key,(unsigned long long)paddr,(unsigned int)count);

	text = request(url,post_data);
	if (!text) {
		fprintf(stderr,"Fail to request %s()\n",page);
		return 0;
	}

	root = json_loads(text, 0, &error);
	if (PRINT_DBG_MSG) printf("return text\n%s\n",text);
	free(text);

	if (!root) {
		fprintf(stderr, "error: on line %d: %s\n", error.line, error.text);
		return 0;
	}

	j_rtn = json_object_get(root, "rtn");
	if (!json_is_integer(j_rtn)) {
		fprintf(stderr, "error: rtn is not an integer\n");
		json_decref(root);
		return 0;
	}

	if (json_integer_value(j_rtn) == 0) { //when read nothing
		json_decref(root);
		return 0;
	}

	if (json_integer_value(j_rtn) < 0) { //when server error occurred
		fprintf(stderr, "error: server error occurred\n");
		json_decref(root);
		return 0;
	}
	unsigned int rtn = (unsigned int) json_integer_value(j_rtn);

	j_buf = json_object_get(root, "buf");
	if (!json_is_string(j_buf)) {
		fprintf(stderr, "error: value is not a string\n");
		json_decref(root);
		return 0;
	}
	char* encodedStr;
	char* decodedStr;
	int encoded_str_size = (int)strlen(json_string_value(j_buf));
	encodedStr = (char*) malloc (encoded_str_size);
	snprintf(encodedStr,encoded_str_size,"%s",json_string_value(j_buf));
	encodedStr[encoded_str_size - 1] = '\0';
	decodedStr = (char*) malloc (encoded_str_size);
	int decodedStrSize = b64_decode_string(encodedStr, decodedStr, encoded_str_size);
	/*int temp_i = 0;
    printf("Json String Value :\n%s",json_string_value(j_buf));
    printf("Json String length : %d\n",encoded_str_size);
    printf("Output %d: \n",call_counter);
    call_counter++;
    int inner_counter = 1;
    for (temp_i=0 ; temp_i < rtn ; temp_i++) {
    	if(decodedStr[temp_i]!=0) {
    		inner_counter++;
    		if(inner_counter % 30 == 0) printf("\n");
    		printf("%d\t%hhX\n", temp_i, decodedStr[temp_i]);
    		printf("%hhX ", decodedStr[temp_i]);
    	}
    }
    printf("\n");
    if (PRINT_DBG_MSG && decodedStrSize != rtn) {
        printf("[warning] rtn value and decoded string size are different!\n");
        printf("rtn value : %u\n",rtn);
        printf("decoded string size : %d\n",decodedStrSize);
    }*/
	memcpy(buf,decodedStr,rtn);
	free(decodedStr);
	json_decref(root);
	return (size_t)rtn;
}

status_t vmi_read_8_ksym(
		vmi_instance_t vmi,
		char *sym,
		uint8_t * value){
	return 0;
}

status_t vmi_read_16_ksym(
		vmi_instance_t vmi,
		char *sym,
		uint16_t * value){
	return 0;
}
status_t vmi_read_32_ksym(
		vmi_instance_t vmi,
		char *sym,
		uint32_t * value){
	return 0;
}

status_t vmi_read_64_ksym(
		vmi_instance_t vmi,
		char *sym,
		uint64_t * value){
	return 0;
}

//TODO
status_t vmi_read_addr_ksym(vmi_instance_t vmi, char *sym, addr_t *value) {
	if (PRINT_DBG_MSG) printf("vmi_read_addr_ksym()\n");
	size_t i;
	char *text;
	char url[URL_SIZE];
	char post_data[POST_BUF_SIZE];
	char *page = "vmi_read_addr_ksym";

	json_t *root;
	json_t *j_rtn;
	json_t *j_value;
	json_error_t error;
	const char *message_text;

	snprintf(url, URL_SIZE, URL_FORMAT, page);
	snprintf(post_data, POST_BUF_SIZE, "%s&vmi_key=%s&sym=%s",O_AUTH_PARAM,vmi->key,sym);

	text = request(url,post_data);
	if (!text)
		return VMI_FAILURE;

	root = json_loads(text, 0, &error);
	if (PRINT_DBG_MSG) printf("return text\n%s\n",text);
	free(text);

	if (!root) {
		fprintf(stderr, "error: on line %d: %s\n", error.line, error.text);
		return VMI_FAILURE;
	}

	j_rtn = json_object_get(root, "rtn");
	if (!json_is_integer(j_rtn)) {
		fprintf(stderr, "error: rtn is not an integer\n");
		json_decref(root);
		return VMI_FAILURE;
	}
	if (json_integer_value(j_rtn)!=VMI_SUCCESS) {
		json_decref(root);
		return VMI_FAILURE;
	}

	j_value = json_object_get(root, "value");
	if (!json_is_string(j_value)) {
		fprintf(stderr, "error: value is not a string\n");
		json_decref(root);
		return VMI_FAILURE;
	}
	char* endptr;
	*value = strtoull(json_string_value(j_value),&endptr,10);
	if(PRINT_DBG_MSG) printf("endptr is %s",endptr);
	json_decref(root);
	return VMI_SUCCESS;
}

char *vmi_read_str_ksym(
		vmi_instance_t vmi,
		char *sym){
	return 0;
}

status_t vmi_read_8_va(
		vmi_instance_t vmi,
		addr_t vaddr,
		int pid,
		uint8_t * value){
	return 0;
}

status_t vmi_read_16_va(
		vmi_instance_t vmi,
		addr_t vaddr,
		int pid,
		uint16_t * value){
	return 0;
}

status_t vmi_read_32_va( vmi_instance_t vmi, addr_t vaddr, int pid, uint32_t * value) {
	if (PRINT_DBG_MSG) printf("vmi_read_32_va()\n");
	size_t i;
	char *text;
	char url[URL_SIZE];
	char post_data[POST_BUF_SIZE];
	char *page = "vmi_read_32_va";

	json_t *root;
	json_t *j_rtn;
	json_t *j_value;
	json_t *vmi_key;
	json_error_t error;
	const char *message_text;

	snprintf(url, URL_SIZE, URL_FORMAT, page);
	snprintf(post_data, POST_BUF_SIZE, "%s&vmi_key=%s&vaddr=%llu&pid=%d",O_AUTH_PARAM,vmi->key,(unsigned long long)vaddr,pid);

	text = request(url,post_data);
	if (!text)
		return VMI_FAILURE;

	root = json_loads(text, 0, &error);
	if (PRINT_DBG_MSG) printf("return text\n%s\n",text);
	free(text);

	if (!root) {
		fprintf(stderr, "error: on line %d: %s\n", error.line, error.text);
		return VMI_FAILURE;
	}

	j_rtn = json_object_get(root, "rtn");
	if (!json_is_integer(j_rtn)) {
		fprintf(stderr, "error: rtn is not an integer\n");
		json_decref(root);
		return VMI_FAILURE;
	}
	if (json_integer_value(j_rtn)!=VMI_SUCCESS) {
		json_decref(root);
		return VMI_FAILURE;
	}

	j_value = json_object_get(root, "value");
	if (!json_is_string(j_value)) {
		fprintf(stderr, "error: value is not a string\n");
		json_decref(root);
		return VMI_FAILURE;
	}
	char* endptr;
	*value = strtoul(json_string_value(j_value),&endptr,10);
	if(PRINT_DBG_MSG) printf("endptr is %s",endptr);
	json_decref(root);
	return VMI_SUCCESS;
}

status_t vmi_read_64_va(
		vmi_instance_t vmi,
		addr_t vaddr,
		int pid,
		uint64_t * value){
	return 0;
}

//TODO
status_t vmi_read_addr_va (vmi_instance_t vmi, addr_t vaddr, int pid, addr_t *value) {
	if (PRINT_DBG_MSG) printf("vmi_read_addr_va()\n");
	size_t i;
	char *text;
	char url[URL_SIZE];
	char post_data[POST_BUF_SIZE];
	char *page = "vmi_read_addr_va";

	json_t *root;
	json_t *j_rtn;
	json_t *j_value;
	json_t *vmi_key;
	json_error_t error;
	const char *message_text;

	snprintf(url, URL_SIZE, URL_FORMAT, page);
	snprintf(post_data, POST_BUF_SIZE, "%s&vmi_key=%s&vaddr=%llu&pid=%d",O_AUTH_PARAM,vmi->key,(unsigned long long)vaddr,pid);

	text = request(url,post_data);
	if (!text)
		return VMI_FAILURE;

	root = json_loads(text, 0, &error);
	if (PRINT_DBG_MSG) printf("return text\n%s\n",text);
	free(text);

	if (!root) {
		fprintf(stderr, "error: on line %d: %s\n", error.line, error.text);
		return VMI_FAILURE;
	}

	j_rtn = json_object_get(root, "rtn");
	if (!json_is_integer(j_rtn)) {
		fprintf(stderr, "error: rtn is not an integer\n");
		json_decref(root);
		return VMI_FAILURE;
	}
	if (json_integer_value(j_rtn)!=VMI_SUCCESS) {
		json_decref(root);
		return VMI_FAILURE;
	}

	j_value = json_object_get(root, "value");
	if (!json_is_string(j_value)) {
		fprintf(stderr, "error: value is not a string\n");
		json_decref(root);
		return VMI_FAILURE;
	}
	char* endptr;
	*value = strtoull(json_string_value(j_value),&endptr,10);
	if(PRINT_DBG_MSG) printf("endptr is %s",endptr);
	json_decref(root);
	return VMI_SUCCESS;
}

//TODO
char *vmi_read_str_va(vmi_instance_t vmi, addr_t vaddr, int pid) {
	if (PRINT_DBG_MSG) printf("vmi_read_str_va()\n");
	size_t i;
	char *text;
	char url[URL_SIZE];
	char post_data[POST_BUF_SIZE];
	char *page = "vmi_read_str_va";

	json_t *root;
	json_t *j_rtn;
	json_t *j_value;
	json_t *vmi_key;
	json_error_t error;
	const char *message_text;

	snprintf(url, URL_SIZE, URL_FORMAT, page);
	snprintf(post_data, POST_BUF_SIZE, "%s&vmi_key=%s&vaddr=%llu&pid=%d",O_AUTH_PARAM,vmi->key,(unsigned long long)vaddr,pid);

	text = request(url,post_data);
	if (!text)
		return NULL;

	root = json_loads(text, 0, &error);
	if (PRINT_DBG_MSG) printf("return text\n%s\n",text);
	free(text);

	if (!root) {
		fprintf(stderr, "error: on line %d: %s\n", error.line, error.text);
		return NULL;
	}

	j_rtn = json_object_get(root, "rtn");
	if (!json_is_string(j_rtn)) {
		fprintf(stderr, "error: rtn is not a string\n");
		json_decref(root);
		return NULL;
	}
	const char *temp_str = json_string_value(j_rtn);
	char *rtn = malloc(strlen(temp_str)+1);
	snprintf(rtn,strlen(temp_str),"%s",temp_str);
	json_decref(root);
	return rtn;
}

unicode_string_t *vmi_read_unicode_str_va(
		vmi_instance_t vmi,
		addr_t vaddr,
		int pid) {

	unicode_string_t *null_struct;
	return null_struct;
}

status_t vmi_convert_str_encoding(
		const unicode_string_t *in,
		unicode_string_t *out,
		const char *outencoding){
	return 0;
}

void vmi_free_unicode_str(
		unicode_string_t *p_us){
	return;
}

status_t vmi_read_8_pa(
		vmi_instance_t vmi,
		addr_t paddr,
		uint8_t * value){
	return 0;
}

status_t vmi_read_16_pa(
		vmi_instance_t vmi,
		addr_t paddr,
		uint16_t * value){
	return 0;
}

status_t vmi_read_32_pa(
		vmi_instance_t vmi,
		addr_t paddr,
		uint32_t * value){
	return 0;
}

status_t vmi_read_64_pa(
		vmi_instance_t vmi,
		addr_t paddr,
		uint64_t * value){
	return 0;
}

status_t vmi_read_addr_pa(
		vmi_instance_t vmi,
		addr_t paddr,
		addr_t *value){
	return 0;
}

char *vmi_read_str_pa(
		vmi_instance_t vmi,
		addr_t paddr){
	return NULL;
}

size_t vmi_write_ksym(
		vmi_instance_t vmi,
		char *sym,
		void *buf,
		size_t count){
	return 0;
}

size_t vmi_write_va(
		vmi_instance_t vmi,
		addr_t vaddr,
		int pid,
		void *buf,
		size_t count){
	return 0;
}

size_t vmi_write_pa(
		vmi_instance_t vmi,
		addr_t paddr,
		void *buf,
		size_t count){
	return 0;
}

status_t vmi_write_8_ksym(
		vmi_instance_t vmi,
		char *sym,
		uint8_t * value){
	return 0;
}

status_t vmi_write_16_ksym(
		vmi_instance_t vmi,
		char *sym,
		uint16_t * value){
	return 0;
}

status_t vmi_write_32_ksym(
		vmi_instance_t vmi,
		char *sym,
		uint32_t * value){
	return 0;
}

status_t vmi_write_64_ksym(
		vmi_instance_t vmi,
		char *sym,
		uint64_t * value){
	return 0;
}

status_t vmi_write_8_va(
		vmi_instance_t vmi,
		addr_t vaddr,
		int pid,
		uint8_t * value){
	return 0;
}

status_t vmi_write_16_va(
		vmi_instance_t vmi,
		addr_t vaddr,
		int pid,
		uint16_t * value){
	return 0;
}

status_t vmi_write_32_va(
		vmi_instance_t vmi,
		addr_t vaddr,
		int pid,
		uint32_t * value){
	return 0;
}

status_t vmi_write_64_va(
		vmi_instance_t vmi,
		addr_t vaddr,
		int pid,
		uint64_t * value){
	return 0;
}

status_t vmi_write_8_pa(
		vmi_instance_t vmi,
		addr_t paddr,
		uint8_t * value){
	return 0;
}

status_t vmi_write_16_pa(
		vmi_instance_t vmi,
		addr_t paddr,
		uint16_t * value){
	return 0;
}

status_t vmi_write_32_pa(
		vmi_instance_t vmi,
		addr_t paddr,
		uint32_t * value){
	return 0;
}

status_t vmi_write_64_pa(
		vmi_instance_t vmi,
		addr_t paddr,
		uint64_t * value){
	return 0;
}

void vmi_print_hex(
		unsigned char *data,
		unsigned long length){
	return;
}

void vmi_print_hex_ksym(
		vmi_instance_t vmi,
		char *sym,
		size_t length){
	return;
}

void vmi_print_hex_va(
		vmi_instance_t vmi,
		addr_t vaddr,
		int pid,
		size_t length){
	return;
}

void vmi_print_hex_pa(
		vmi_instance_t vmi,
		addr_t paddr,
		size_t length){
	return;
}

char *vmi_get_name( vmi_instance_t vmi) {
	if (PRINT_DBG_MSG) printf("vmi_get_name()\n");
	size_t i;
	char *text;
	char url[URL_SIZE];
	char post_data[POST_BUF_SIZE];
	char *page = "vmi_get_name";

	json_t *root;
	json_t *j_rtn;
	json_error_t error;
	const char *message_text;

	snprintf(url, URL_SIZE, URL_FORMAT, page);
	snprintf(post_data, POST_BUF_SIZE, "%s&vmi_key=%s",O_AUTH_PARAM,vmi->key);

	text = request(url,post_data);
	if (!text)
		return NULL;

	root = json_loads(text, 0, &error);
	if (PRINT_DBG_MSG) printf("return text\n%s\n",text);
	free(text);

	if (!root) {
		fprintf(stderr, "error: on line %d: %s\n", error.line, error.text);
		return NULL;
	}

	j_rtn = json_object_get(root, "rtn");
	if (!json_is_string(j_rtn)) {
		fprintf(stderr, "error: rtn is not a string\n");
		json_decref(root);
		return NULL;
	}
	const char *temp_str = json_string_value(j_rtn);
	char *rtn = malloc(strlen(temp_str)+1);
	snprintf(rtn,strlen(temp_str),"%s",temp_str);
	json_decref(root);
	return rtn;
}

//TODO
unsigned long vmi_get_vmid(vmi_instance_t vmi) {
	if (PRINT_DBG_MSG) printf("vmi_get_vmid()\n");
	size_t i;
	char *text;
	char url[URL_SIZE];
	char post_data[POST_BUF_SIZE];
	char *page = "vmi_get_vmid";

	json_t *root;
	json_t *j_rtn;
	json_error_t error;
	const char *message_text;

	unsigned long target_vmid;

	snprintf(url, URL_SIZE, URL_FORMAT, page);
	snprintf(post_data, POST_BUF_SIZE, "%s&vmi_key=%s",O_AUTH_PARAM,vmi->key);

	text = request(url,post_data);
	if (!text){
		fprintf(stderr, "error: fail to request vmi_get_vmid()\n");
		return 1;
	}

	root = json_loads(text, 0, &error);
	if (PRINT_DBG_MSG) printf("return text\n%s\n",text);
	free(text);

	if (!root) {
		fprintf(stderr, "error: on line %d: %s\n", error.line, error.text);
		return 1;
	}

	j_rtn = json_object_get(root, "rtn");
	if (!json_is_string(j_rtn)) {
		fprintf(stderr, "error: rtn is not a string\n");
		json_decref(root);
		return 1;
	}

	char* endptr;
	target_vmid = strtoul(json_string_value(j_rtn),&endptr,10);
	if(PRINT_DBG_MSG) printf("endptr is %s",endptr);
	json_decref(root);
	return target_vmid;
}

//TODO
uint32_t vmi_get_access_mode( vmi_instance_t vmi ) {
	if (PRINT_DBG_MSG) printf("vmi_get_access_mode()\n");
	size_t i;
	char *text;
	char url[URL_SIZE];
	char post_data[POST_BUF_SIZE];
	char *page = "vmi_get_access_mode";

	json_t *root;
	json_t *j_rtn;
	json_error_t error;
	const char *message_text;

	uint32_t target_access_mode;

	snprintf(url, URL_SIZE, URL_FORMAT, page);
	snprintf(post_data, POST_BUF_SIZE, "%s&vmi_key=%s",O_AUTH_PARAM,vmi->key);

	text = request(url,post_data);
	if (!text){
		fprintf(stderr, "error: fail to request vmi_get_access_mode()\n");
		return -1;
	}

	root = json_loads(text, 0, &error);
	if (PRINT_DBG_MSG) printf("return text\n%s\n",text);
	free(text);

	if (!root) {
		fprintf(stderr, "error: on line %d: %s\n", error.line, error.text);
		return -1;
	}

	j_rtn = json_object_get(root, "rtn");
	if (!json_is_integer(j_rtn)) {
		fprintf(stderr, "error: rtn is not an integer\n");
		json_decref(root);
		return -1;
	}

	target_access_mode = json_integer_value(j_rtn);
	json_decref(root);
	return target_access_mode;
}

//TODO
page_mode_t vmi_get_page_mode( vmi_instance_t vmi ) {
	if (PRINT_DBG_MSG) printf("vmi_get_page_mode()\n");
	size_t i;
	char *text;
	char url[URL_SIZE];
	char post_data[POST_BUF_SIZE];
	char *page = "vmi_get_page_mode";

	json_t *root;
	json_t *j_rtn;
	json_error_t error;
	const char *message_text;

	page_mode_t target_page_mode;

	snprintf(url, URL_SIZE, URL_FORMAT, page);
	snprintf(post_data, POST_BUF_SIZE, "%s&vmi_key=%s",O_AUTH_PARAM,vmi->key);

	text = request(url,post_data);
	if (!text)
		return -1;

	root = json_loads(text, 0, &error);
	if (PRINT_DBG_MSG) printf("return text\n%s\n",text);
	free(text);

	if (!root) {
		fprintf(stderr, "error: on line %d: %s\n", error.line, error.text);
		return -1;
	}

	j_rtn = json_object_get(root, "rtn");
	if (!json_is_integer(j_rtn)) {
		fprintf(stderr, "error: rtn is not an integer\n");
		json_decref(root);
		return -1;
	}

	target_page_mode = json_integer_value(j_rtn);
	json_decref(root);
	return target_page_mode;
}

//TODO
//returns -1 when error occured.
os_t vmi_get_ostype(vmi_instance_t vmi) {
	if (PRINT_DBG_MSG) printf("vmi_get_ostype()\n");
	size_t i;
	char *text;
	char url[URL_SIZE];
	char post_data[POST_BUF_SIZE];
	char *page = "vmi_get_ostype";

	json_t *root;
	json_t *rtn;
	json_t *vmi_key;
	json_error_t error;
	const char *message_text;

	os_t target_os_type;

	snprintf(url, URL_SIZE, URL_FORMAT, page);
	snprintf(post_data, POST_BUF_SIZE, "%s&vmi_key=%s",O_AUTH_PARAM,vmi->key);

	text = request(url,post_data);
	if (!text)
		return -1;

	root = json_loads(text, 0, &error);
	if (PRINT_DBG_MSG) printf("return text\n%s\n",text);
	free(text);

	if (!root) {
		fprintf(stderr, "error: on line %d: %s\n", error.line, error.text);
		return -1;
	}
	rtn = json_object_get(root, "rtn");
	if (!json_is_integer(rtn)) {
		fprintf(stderr, "error: rtn is not an integer\n");
		json_decref(root);
		return -1;
	}
	target_os_type = json_integer_value(rtn);
	json_decref(root);
	return target_os_type;
}

win_ver_t vmi_get_winver(
		vmi_instance_t vmi){
	return 0;
}

const char *vmi_get_winver_str(
		vmi_instance_t vmi){
	return 0;
}

win_ver_t vmi_get_winver_manual(
		vmi_instance_t vmi,
		addr_t kdvb_pa){
	return 0;
}

//TODO
unsigned long vmi_get_offset( vmi_instance_t vmi, char *offset_name) {
	if (PRINT_DBG_MSG) printf("vmi_get_offset()\n");
	size_t i;
	char *text;
	char url[URL_SIZE];
	char post_data[POST_BUF_SIZE];
	char *page = "vmi_get_offset";

	json_t *root;
	json_t *j_rtn;
	json_error_t error;

	unsigned long target_offset;

	snprintf(url, URL_SIZE, URL_FORMAT, page);
	snprintf(post_data, POST_BUF_SIZE, "%s&vmi_key=%s&offset_name=%s",O_AUTH_PARAM,vmi->key,offset_name);

	text = request(url,post_data);
	if (!text){
		fprintf(stderr, "error: fail to request vmi_get_offset()\n");
		return 1;
	}

	root = json_loads(text, 0, &error);
	if (PRINT_DBG_MSG) printf("return text\n%s\n",text);
	free(text);

	if (!root) {
		fprintf(stderr, "error: on line %d: %s\n", error.line, error.text);
		return 1;
	}
	j_rtn = json_object_get(root, "rtn");
	if (!json_is_string(j_rtn)) {
		fprintf(stderr, "error: rtn is not a string\n");
		json_decref(root);
		return 1;
	}
	char* endptr;
	//printf("String : %s\n",json_string_value(j_rtn));
	target_offset = strtoul(json_string_value(j_rtn),&endptr,10);
	//printf("integer : %lu\n",target_offset);
	if(PRINT_DBG_MSG) printf("endptr is %s",endptr);
	json_decref(root);
	return target_offset;
}

//TODO
unsigned long vmi_get_memsize( vmi_instance_t vmi) {
	if (PRINT_DBG_MSG) printf("vmi_get_memsize()\n");
	size_t i;
	char *text;
	char url[URL_SIZE];
	char post_data[POST_BUF_SIZE];
	char *page = "vmi_get_memsize";

	json_t *root;
	json_t *j_rtn;
	json_error_t error;

	unsigned long target_memsize;

	snprintf(url, URL_SIZE, URL_FORMAT, page);
	snprintf(post_data, POST_BUF_SIZE, "%s&vmi_key=%s",O_AUTH_PARAM,vmi->key);
	text = request(url,post_data);
	if (!text){
		fprintf(stderr, "error: fail to request vmi_get_memsize()\n");
		return 1;
	}

	root = json_loads(text, 0, &error);
	if (PRINT_DBG_MSG) printf("return text\n%s\n",text);
	free(text);

	if (!root) {
		fprintf(stderr, "error: on line %d: %s\n", error.line, error.text);
		return 1;
	}

	j_rtn = json_object_get(root, "rtn");
	if (!json_is_string(j_rtn)) {
		fprintf(stderr, "error: rtn is not a string\n");
		json_decref(root);
		return 1;
	}
	char* endptr;
	target_memsize = strtoul(json_string_value(j_rtn),&endptr,10);
	if(PRINT_DBG_MSG) printf("endptr is %s",endptr);
	json_decref(root);
	return target_memsize;
}

unsigned int vmi_get_num_vcpus (
		vmi_instance_t vmi){
	return 0;
}

status_t vmi_get_vcpureg(
		vmi_instance_t vmi,
		reg_t *value,
		registers_t reg,
		unsigned long vcpu){
	return 0;
}

//TODO
status_t vmi_pause_vm(vmi_instance_t vmi){
	if (PRINT_DBG_MSG) printf("vmi_pause_vm()\n");
	size_t i;
	char *text;
	char url[URL_SIZE];
	char post_data[POST_BUF_SIZE];
	char *page = "vmi_pause_vm";

	json_t *root;
	json_t *rtn;
	json_error_t error;
	const char *message_text;

	snprintf(url, URL_SIZE, URL_FORMAT, page);
	snprintf(post_data, POST_BUF_SIZE, "%s&vmi_key=%s",O_AUTH_PARAM, vmi->key);

	text = request(url,post_data);
	if (!text)
		return VMI_FAILURE;

	root = json_loads(text, 0, &error);
	if (PRINT_DBG_MSG) printf("return text\n%s\n",text);
	free(text);

	if (!root) {
		fprintf(stderr, "error: on line %d: %s\n", error.line, error.text);
		return VMI_FAILURE;
	}

	rtn = json_object_get(root, "rtn");
	if (!json_is_integer(rtn)) {
		fprintf(stderr, "error: rtn is not an integer\n");
		json_decref(root);
		return VMI_FAILURE;
	}
	if (json_integer_value(rtn)!=VMI_SUCCESS) {
		json_decref(root);
		return VMI_FAILURE;
	}
	json_decref(root);
	return VMI_SUCCESS;
}

//TODO
status_t vmi_resume_vm(vmi_instance_t vmi) {
	if (PRINT_DBG_MSG) printf("vmi_resume_vm()\n");
	size_t i;
	char *text;
	char url[URL_SIZE];
	char post_data[POST_BUF_SIZE];
	char *page = "vmi_resume_vm";

	json_t *root;
	json_t *rtn;
	json_error_t error;
	const char *message_text;

	snprintf(url, URL_SIZE, URL_FORMAT, page);
	snprintf(post_data, POST_BUF_SIZE, "%s&vmi_key=%s",O_AUTH_PARAM,vmi->key);

	text = request(url,post_data);
	if (!text)
		return VMI_FAILURE;

	root = json_loads(text, 0, &error);
	if (PRINT_DBG_MSG) printf("return text\n%s\n",text);
	free(text);

	if (!root) {
		fprintf(stderr, "error: on line %d: %s\n", error.line, error.text);
		return VMI_FAILURE;
	}

	rtn = json_object_get(root, "rtn");
	if (!json_is_integer(rtn)) {
		fprintf(stderr, "error: rtn is not an integer\n");
		json_decref(root);
		return VMI_FAILURE;
	}
	if (json_integer_value(rtn)!=VMI_SUCCESS) {
		json_decref(root);
		return VMI_FAILURE;
	}
	json_decref(root);
	return VMI_SUCCESS;
}

void vmi_v2pcache_flush(
		vmi_instance_t vmi){
	return;
}

void vmi_v2pcache_add(
		vmi_instance_t vmi,
		addr_t va,
		addr_t dtb,
		addr_t pa){
	return;
}

void vmi_symcache_flush(
		vmi_instance_t vmi){
	return;
}

void vmi_symcache_add(
		vmi_instance_t vmi,
		char *sym,
		addr_t va){
	return;
}

void vmi_pidcache_flush(
		vmi_instance_t vmi){
	return;
}

void vmi_pidcache_add(
		vmi_instance_t vmi,
		int pid,
		addr_t dtb){
	return;
}

status_t vmi_handle_event(
		vmi_instance_t vmi,
		vmi_event_t event,
		event_callback_t callback){
	return 0;
}
status_t vmi_clear_event(
		vmi_instance_t vmi,
		vmi_event_t event){
	return 0;
}

status_t vmi_events_listen(
		vmi_instance_t vmi,
		uint32_t timeout){
	return 0;
}

/*
 * This code originally came from here
 * https://github.com/davidgaleano/libwebsockets/blob/master/lib/base64-decode.c
 * which is from here
 * http://base64.sourceforge.net/b64.c
 * with the following license:
 *
 * LICENCE:        Copyright (c) 2001 Bob Trower, Trantor Standard Systems Inc.
 *
 *                Permission is hereby granted, free of charge, to any person
 *                obtaining a copy of this software and associated
 *                documentation files (the "Software"), to deal in the
 *                Software without restriction, including without limitation
 *                the rights to use, copy, modify, merge, publish, distribute,
 *                sublicense, and/or sell copies of the Software, and to
 *                permit persons to whom the Software is furnished to do so,
 *                subject to the following conditions:
 *
 *                The above copyright notice and this permission notice shall
 *                be included in all copies or substantial portions of the
 *                Software.
 *
 *                THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY
 *                KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 *                WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 *                PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
 *                OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 *                OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 *                OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 *                SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * VERSION HISTORY:
 *               Bob Trower 08/04/01 -- Create Version 0.00.00B
 *
 * COMMENTS FROM davidgaleano :
 *
 * I cleaned it up quite a bit to match the (linux kernel) style of the rest
 * of libwebsockets; this version is under LGPL2 like the rest of libwebsockets
 * since he explictly allows sublicensing, but I give the URL above so you can
 * get the original with Bob's super-liberal terms directly if you prefer.
 *
 */

static const char encode[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
		"abcdefghijklmnopqrstuvwxyz0123456789+/";
static const char decode[] = "|$$$}rstuvwxyz{$$$$$$$>?@ABCDEFGHIJKLMNOPQRSTUVW"
		"$$$$$$XYZ[\\]^_`abcdefghijklmnopq";
int b64_encode_string(const char *in, int in_len, char *out, int out_size) {
	unsigned char triple[3];
	int i;
	int len;
	int line = 0;
	int done = 0;
	while (in_len) {
		len = 0;
		for (i = 0; i < 3; i++) {
			if (in_len) {
				triple[i] = *in++;
				len++;
				in_len--;
			} else
				triple[i] = 0;
		}
		if (len) {
			if (done + 4 >= out_size)
				return -1;
			*out++ = encode[triple[0] >> 2];
			*out++ = encode[((triple[0] & 0x03) << 4) |
			                ((triple[1] & 0xf0) >> 4)];
			*out++ = (len > 1 ? encode[((triple[1] & 0x0f) << 2) |
			                           ((triple[2] & 0xc0) >> 6)] : '=');
			*out++ = (len > 2 ? encode[triple[2] & 0x3f] : '=');
			done += 4;
			line += 4;
		}
	}
	if (done + 1 >= out_size)
		return -1;
	*out++ = '\0';
	return done;
}

/*
 * returns length of decoded string in out, or -1 if out was too small
 * according to out_size
 */

int b64_decode_string(const char *in, char *out, int out_size) {
	int len;
	int i;
	int done = 0;
	unsigned char v;
	unsigned char quad[4];
	while (*in) {
		len = 0;
		for (i = 0; i < 4 && *in; i++) {
			v = 0;
			while (*in && !v) {
				v = *in++;
				v = (v < 43 || v > 122) ? 0 : decode[v - 43];
				if (v)
					v = (v == '$') ? 0 : v - 61;
				if (*in) {
					len++;
					if (v)
						quad[i] = v - 1;
				} else
					quad[i] = 0;
			}
		}
		if (!len)
			continue;
		if (out_size < (done + len - 1))
			/* out buffer is too small */
			return -1;
		if (len >= 2)
			*out++ = quad[0] << 2 | quad[1] >> 4;
		if (len >= 3)
			*out++ = quad[1] << 4 | quad[2] >> 2;
		if (len >= 4)
			*out++ = ((quad[2] << 6) & 0xc0) | quad[3];
		done += len - 1;
	}
	if (done + 1 >= out_size)
		return -1;
	*out++ = '\0';
	return done;
}
#endif //end of __USE_RESTFUL__
